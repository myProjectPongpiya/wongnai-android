package com.wongnai.appdata.repository.api.coin

import com.wongnai.appdata.repository.response.coin.CoinDetailResponse
import com.wongnai.appdata.repository.response.coin.CoinResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface CoinApi {
    @GET("coins")
    suspend fun getCoin(): CoinResponse

    @GET("coin/{uuid}")
    suspend fun getCoinDetail(@Path("uuid") uuid: String): CoinDetailResponse

    @GET("coins?")
    suspend fun onSearch(@Query("search") keyword: String): CoinResponse
}
