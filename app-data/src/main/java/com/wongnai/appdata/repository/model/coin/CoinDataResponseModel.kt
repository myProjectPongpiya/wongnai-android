package com.wongnai.appdata.repository.model.coin

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CoinDataResponseModel(
    @SerializedName("stats")
    val stats: Stats,
    @SerializedName("coins")
    val coins: List<Coin>
) : Parcelable {

    @Parcelize
    data class Stats(
        @SerializedName("total")
        val total: Long,
        @SerializedName("referenceCurrencyRate")
        val referenceCurrencyRate: Long,
        @SerializedName("totalCoins")
        val totalCoins: Long,
        @SerializedName("totalMarkets")
        val totalMarkets: Long,
        @SerializedName("totalExchanges")
        val totalExchanges: Long,
        @SerializedName("totalMarketCap")
        val totalMarketCap: String,
        @SerializedName("total24hVolume")
        val total24HVolume: String,
        @SerializedName("btcDominance")
        val btcDominance: Double,
        @SerializedName("bestCoins")
        val bestCoins: List<EstCoin>,
        @SerializedName("newestCoins")
        val newestCoins: List<EstCoin>
    ) : Parcelable

    @Parcelize
    data class EstCoin(
        @SerializedName("uuid")
        val uuid: String,
        @SerializedName("symbol")
        val symbol: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("iconUrl")
        val iconURL: String,
        @SerializedName("coinrankingUrl")
        val coinrankingURL: String
    ) : Parcelable

    @Parcelize
    data class Coin(
        @SerializedName("uuid")
        val uuid: String,
        @SerializedName("symbol")
        val symbol: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("color")
        val color: String? = null,
        @SerializedName("iconUrl")
        val iconURL: String,
        @SerializedName("marketCap")
        val marketCap: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("listedAt")
        val listedAt: Long,
        @SerializedName("tier")
        val tier: Long,
        @SerializedName("change")
        val change: String,
        @SerializedName("rank")
        val rank: Long,
        @SerializedName("sparkline")
        val sparkline: List<String>,
        @SerializedName("lowVolume")
        val lowVolume: Boolean,
        @SerializedName("coinrankingURL")
        val coinrankingURL: String,
        @SerializedName("24hVolume")
        val the24HVolume: String,
        @SerializedName("btcPrice")
        val btcPrice: String
    ) : Parcelable


}
