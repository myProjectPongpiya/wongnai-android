package com.wongnai.appdata.repository.model.coin

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CoinDetailResponseModel(
    @SerializedName("coin")
    val coin: CoinDetail
) : Parcelable

@Parcelize
data class CoinDetail(
    val uuid: String,
    val symbol: String,
    val name: String,
    val description: String,
    val color: String,

    @SerializedName("iconUrl")
    val iconURL: String,

    @SerializedName("websiteUrl")
    val websiteURL: String,

    val links: List<Link>,
    val supply: Supply,
    val numberOfMarkets: Long,
    val numberOfExchanges: Long,

    @SerializedName("24hVolume")
    val the24HVolume: String,

    val marketCap: String,
    val price: String,
    val btcPrice: String,
    val priceAt: Long,
    val change: String,
    val rank: Long,
    val sparkline: List<String>,
    val allTimeHigh: AllTimeHigh,

    @SerializedName("coinrankingUrl")
    val coinrankingURL: String,

    val tier: Long,
    val lowVolume: Boolean,
    val listedAt: Long
) : Parcelable

@Parcelize
data class AllTimeHigh(
    val price: String,
    val timestamp: Long
) : Parcelable

@Parcelize
data class Link(
    val name: String,
    val type: String,
    val url: String
) : Parcelable

@Parcelize
data class Supply(
    val confirmed: Boolean,
    val total: String,
    val circulating: String
) : Parcelable


