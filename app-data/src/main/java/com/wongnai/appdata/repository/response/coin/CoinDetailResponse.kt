package com.wongnai.appdata.repository.response.coin

import com.google.gson.annotations.SerializedName
import com.wongnai.appdata.repository.model.coin.CoinDataResponseModel
import com.wongnai.appdata.repository.model.coin.CoinDetailResponseModel

data class CoinDetailResponse(
  @SerializedName("status")
  val status: String? = null,
  @SerializedName("data")
  val data: CoinDetailResponseModel
)
