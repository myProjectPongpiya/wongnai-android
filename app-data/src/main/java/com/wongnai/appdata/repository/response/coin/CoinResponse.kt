package com.wongnai.appdata.repository.response.coin

import com.google.gson.annotations.SerializedName
import com.wongnai.appdata.repository.model.coin.CoinDataResponseModel

data class CoinResponse(
  @SerializedName("status")
  val status: String? = null,
  @SerializedName("data")
  val data: CoinDataResponseModel
)
