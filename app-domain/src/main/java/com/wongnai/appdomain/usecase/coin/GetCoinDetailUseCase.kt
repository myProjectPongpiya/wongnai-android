package com.wongnai.appdomain.usecase.coin

import com.wongnai.appdata.repository.api.coin.CoinApi
import com.wongnai.appdata.repository.response.coin.CoinDetailResponse
import com.wongnai.appdata.repository.response.coin.CoinResponse
import com.wongnai.appfoundation.base.BaseUseCase
import com.wongnai.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetCoinDetailUseCase(
    private val repository: CoinApi
) : BaseUseCase<String, CoinDetailResponse>() {
    override fun onBuild(params: String): Flow<CoinDetailResponse> {
        return flowSingle { repository.getCoinDetail(params) }
    }
}