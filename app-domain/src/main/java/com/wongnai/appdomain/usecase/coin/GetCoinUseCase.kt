package com.wongnai.appdomain.usecase.coin

import com.wongnai.appdata.repository.api.coin.CoinApi
import com.wongnai.appdata.repository.response.coin.CoinResponse
import com.wongnai.appfoundation.base.BaseUseCase
import com.wongnai.appfoundation.extensions.flowSingle
import kotlinx.coroutines.flow.Flow

class GetCoinUseCase(
    private val repository: CoinApi
) : BaseUseCase<Unit, CoinResponse>() {
    override fun onBuild(params: Unit): Flow<CoinResponse> {
        return flowSingle { repository.getCoin() }
    }
}