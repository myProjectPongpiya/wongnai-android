package com.wongnai.appfoundation.authentication

object Endpoint {

    /**
     * Indicates that this endpoint should send the OAuth client credentials
     * instead of the standard access token.
     */
    @Retention(AnnotationRetention.RUNTIME)
    annotation class RequiresOauth

    @Retention(AnnotationRetention.RUNTIME)
    annotation class SkipAuthScope
}
