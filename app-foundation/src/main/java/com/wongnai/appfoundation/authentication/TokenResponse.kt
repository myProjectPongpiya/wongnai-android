package com.wongnai.appfoundation.authentication

import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class TokenResponse(
    @SerializedName("content")
    val content: TokenResponseModel? = null,
) {

    data class TokenResponseModel(
        /**
         * The access_token code.
         */
        @SerializedName("accessToken")
        val accessToken: String,

        /**
         * The refresh token code
         */
        @SerializedName("refreshToken")
        val refreshToken: String,
    )
}
