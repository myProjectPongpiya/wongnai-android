package com.wongnai.appfoundation.authentication

interface TokenService {

    fun getToken(): AuthorizationToken?

    fun setToken(token: AuthorizationToken)

    fun clear()
}
