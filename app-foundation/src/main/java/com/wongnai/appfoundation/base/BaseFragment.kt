package com.wongnai.appfoundation.base

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.wongnai.appfoundation.permissions.PermissionResult
import com.wongnai.appfoundation.text.CustomSpannable
import com.wongnai.appfoundation.ui.Alert
import com.wongnai.appfoundation.ui.UIController
import timber.log.Timber
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

abstract class BaseFragment : Fragment(), UIController {

    override val disposeBag: MutableList<() -> Unit>
        get() = mutableListOf()

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    val onViewCreatedListeners = mutableListOf<(View, Bundle?) -> Unit>()

    private fun navController() = findNavController()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreatedListeners.forEach { it.invoke(view, savedInstanceState) }
    }

    override fun onDestroyView() {
        disposeBag.forEach { it.invoke() }
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        onViewCreatedListeners.clear()
    }

    override fun setLoading(isLoading: Boolean, vararg labels: String) {
        (this.activity as? UIController)?.setLoading(isLoading, *labels)
    }

    override fun showToast(message: String, toastType: Alert.ToastType) {
        (this.activity as? UIController)?.showToast(message, toastType)
    }

    override fun showPopup(
        title: String?,
        message: String?,
        subMessage: CustomSpannable?,
        buttonLabel: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?
    ) {
        (this.activity as? UIController)?.showPopup(
            title,
            message,
            subMessage,
            buttonLabel,
            heroImage,
            onDismiss
        )
    }

    override fun showPopupWithMultiButton(
        title: String?,
        message: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
        multiButtonType: Alert.MultiButtonType
    ) {
        (this.activity as? UIController)?.showPopupWithMultiButton(
            title,
            message,
            heroImage,
            onDismiss,
            negativeButtonLabel,
            negativeButtonListener,
            positiveButtonLabel,
            positiveButtonListener,
            multiButtonType
        )
    }

    override fun showNativePopup(
        title: String?,
        message: String?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
    ) {
        (this.activity as? UIController)?.showNativePopup(
            title,
            message,
            positiveButtonLabel,
            positiveButtonListener,
            negativeButtonLabel,
            negativeButtonListener
        )
    }

    override fun launchDeepLink(uri: String, internalOnly: Boolean, onNotSupported: (() -> Unit)?) {
        (this.activity as? UIController)?.launchDeepLink(uri, internalOnly, onNotSupported)
    }

    override fun showNoConnectionError(onConnected: () -> Unit) {
        (this.activity as? UIController)?.showNoConnectionError(onConnected)
    }

    override fun showPopupContactToMedicalCouncil(title: String) {
        (this.activity as? UIController)?.showPopupContactToMedicalCouncil(title)
    }

    override fun restartSession() {
        (this.activity as? UIController)?.restartSession()
    }

    override fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)?,
        onGranted: () -> Unit
    ) {
        (this.activity as? UIController)?.requestPermissions(
            *permissions,
            onDenied = onDenied,
            onGranted = onGranted
        )
    }

    override fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit
    ) {
        (this.activity as? UIController)?.requestPermissionsWithRationale(
            *permissions,
            onResult = onResult
        )
    }

    override fun gotoPage(navDirections: NavDirections) {
        Timber.d(
            "currentDestination = %s, page=%s",
            navController().currentDestination?.id,
            navDirections.actionId
        )
        try {
            navController()
                .navigate(navDirections)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    override fun goBack(@IdRes destinationId: Int?) {
        if (destinationId == null) {
            navController().popBackStack()
        } else {
            try {
                navController()
                    .popBackStack(destinationId, false)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    override fun onCallPhoneNumber(phoneNo: String) {
        (this.activity as? UIController)?.onCallPhoneNumber(phoneNo)
    }

    override fun onCallEmail(email: String) {
        (this.activity as? UIController)?.onCallEmail(email)
    }

    override fun onCallWebBrowser(link: String) {
        (this.activity as? UIController)?.onCallWebBrowser(link)
    }

    override fun onDeeplinkPreRoute(continuation: Continuation<Unit>) {
        continuation.resume(Unit)
    }

    /**
     * Set window soft input mode for the screen.
     */

    protected fun setWindowSoftInputMode(mode: Int) {
        (this.activity as? BaseActivity)?.setWindowSoftInputMode(mode)
    }

    companion object {
        private const val TAG = "BaseFragment"
    }
}
