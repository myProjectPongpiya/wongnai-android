package com.wongnai.appfoundation.base

import androidx.annotation.IdRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.wongnai.appfoundation.deeplink.DeepLinkEvent
import com.wongnai.appfoundation.http.ApiException
import com.wongnai.appfoundation.model.LoadingState
import com.wongnai.appfoundation.model.MutableLiveEvent
import com.wongnai.appfoundation.permissions.PermissionRequest
import com.wongnai.appfoundation.permissions.PermissionResult
import com.wongnai.appfoundation.permissions.PermissionUtils
import com.wongnai.appfoundation.text.CustomSpannable
import com.wongnai.appfoundation.ui.Alert
import com.wongnai.appfoundation.ui.UIController
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

/**
 * Provides a [ViewModel] with several preset observables along with additional
 * helpers and lifecycle methods.
 */

open class BaseViewModel : ViewModel(), UIController {

    override val coroutineContext: CoroutineContext
        get() = viewModelScope.coroutineContext

    override val disposeBag: MutableList<() -> Unit>
        get() = mutableListOf()

    // COMMON STATE

    val loadingState = MutableLiveData<LoadingState>()

    // GOTO

    val gotoPage = MutableLiveEvent<NavDirections>()
    val goBack = MutableLiveEvent<Int?>()

    // EVENTS

    val onLaunchDeepLinkEvent = MutableLiveEvent<DeepLinkEvent>()
    val onNoConnectionEvent = MutableLiveEvent<NoConnectionEvent>()
    val onContactMedicalCouncilEvent = MutableLiveEvent<String>()
    val onRestartSession = MutableLiveEvent<ApiException>()
    val onAlertEvent = MutableLiveEvent<Alert>()
    val onRequestPermissionEvent = MutableLiveEvent<PermissionRequest>()
    val onCallPhoneNumber = MutableLiveEvent<String>()
    val onCallEmail = MutableLiveEvent<String>()
    val onCallWebBrowser = MutableLiveEvent<String>()

    fun addDisposableInternal(d: Disposable) {
        this.disposeBag.add { d.dispose() }
    }

    override fun onCleared() {
        disposeBag.forEach { it.invoke() }
        super.onCleared()
    }

    override fun showToast(message: String, toastType: Alert.ToastType) {
        val toast = Alert.Toast(message, toastType)
        launch {
            delay(DELEY)
            onAlertEvent.emit(toast)
        }
    }

    override fun showPopup(
        title: String?,
        message: String?,
        subMessage: CustomSpannable?,
        buttonLabel: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?
    ) {
        val alert = Alert.Popup(title, message, subMessage, buttonLabel, heroImage, onDismiss)
        launch {
            delay(DELEY)
            onAlertEvent.emit(alert)
        }
    }

    override fun showPopupWithMultiButton(
        title: String?,
        message: String?,
        heroImage: Int?,
        onDismiss: (() -> Unit)?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
        multiButtonType: Alert.MultiButtonType
    ) {
        val alert = Alert.PopupMultiButton(
            title,
            message,
            heroImage,
            onDismiss,
            negativeButtonLabel,
            negativeButtonListener,
            positiveButtonLabel,
            positiveButtonListener,
            multiButtonType
        )
        launch {
            delay(DELEY)
            onAlertEvent.emit(alert)
        }
    }

    override fun showNativePopup(
        title: String?,
        message: String?,
        negativeButtonLabel: String?,
        negativeButtonListener: (() -> Unit)?,
        positiveButtonLabel: String?,
        positiveButtonListener: (() -> Unit)?,
    ) {
        val alert = Alert.NativePopup(
            title = title,
            message = message,
            negativeButtonLabel = negativeButtonLabel,
            negativeButtonListener = negativeButtonListener,
            positiveButtonLabel = positiveButtonLabel,
            positiveButtonListener = positiveButtonListener,
        )
        launch {
            delay(DELEY)
            onAlertEvent.emit(alert)
        }
    }

    override fun setLoading(isLoading: Boolean, vararg labels: String) {
        this.loadingState.value = LoadingState(
            isLoading = isLoading,
            labels = labels.toList()
        )
    }

    override fun gotoPage(navDirections: NavDirections) {
        this.gotoPage.emit(navDirections)
    }

    override fun goBack(@IdRes destinationId: Int?) {
        this.goBack.emit(destinationId)
    }

    override fun onCallPhoneNumber(phone: String) {
        this.onCallPhoneNumber.emit(phone)
    }

    override fun onCallEmail(email: String) {
        this.onCallEmail.emit(email)
    }

    override fun onCallWebBrowser(link: String) {
        this.onCallWebBrowser.emit(link)
    }

    override fun onDeeplinkPreRoute(continuation: Continuation<Unit>) {
        continuation.resume(Unit)
    }

    override fun launchDeepLink(
        uri: String,
        internalOnly: Boolean,
        onNotSupported: (() -> Unit)?
    ) {
        onLaunchDeepLinkEvent.emit(DeepLinkEvent(uri, internalOnly, onNotSupported))
    }

    override fun showNoConnectionError(onConnected: () -> Unit) {
        onNoConnectionEvent.emit(NoConnectionEvent(onConnected))
    }

    override fun showPopupContactToMedicalCouncil(title: String) {
        onContactMedicalCouncilEvent.emit(title)
    }

    override fun restartSession() {
        onRestartSession.call()
    }

    override fun requestPermissions(
        vararg permissions: String,
        onDenied: (() -> Unit)?,
        onGranted: () -> Unit
    ) {
        val request = PermissionRequest(permissions.toList()) {
            when (it) {
                is PermissionResult.RationaleRequired -> PermissionUtils.onAskPermissions(it)
                is PermissionResult.Denied -> onDenied?.invoke()
                is PermissionResult.Granted -> onGranted.invoke()
            }
        }
        onRequestPermissionEvent.emit(request)
    }

    override fun requestPermissionsWithRationale(
        vararg permissions: String,
        onResult: (PermissionResult) -> Unit
    ) {
        onRequestPermissionEvent.emit(PermissionRequest(permissions.toList(), onResult))
    }

    data class NoConnectionEvent(val onConnected: () -> Unit)

    companion object {
        private const val DELEY = 250L
    }
}
