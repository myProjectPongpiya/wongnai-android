package com.wongnai.appfoundation.crypto.manager

import org.bouncycastle.util.encoders.Base64
import java.security.KeyFactory
import java.security.PublicKey
import java.security.SecureRandom
import java.security.spec.X509EncodedKeySpec

class KeyProviderImp(
    private val publicKeyPEM: String
) : KeyService {

    override fun getRandomSeed(): CharArray = RANDOM_SEED.toCharArray()

    override fun getRandomKey(length: Int): String {
        val random = SecureRandom()
        val randomKeyBuilder = StringBuilder(length)
        val seed = getRandomSeed()
        for (keyIndex in 0 until length) {
            randomKeyBuilder.append(seed[random.nextInt(seed.size)])
        }
        return randomKeyBuilder.toString()
    }

    override fun getPEMPublicKey(): PublicKey {
        val decoded = Base64.decode(publicKeyPEM.toByteArray(Charsets.UTF_8))
        val keyFactory = KeyFactory.getInstance(ALGORITHM)
        val keySpec = X509EncodedKeySpec(decoded)

        return keyFactory.generatePublic(keySpec)
    }

    override fun getIteration(): Int = ITERATIONS

    companion object {
        private const val ALGORITHM = "RSA"
        private const val RANDOM_SEED =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        private const val ITERATIONS = 1
    }
}
