package com.wongnai.appfoundation.data

import android.content.SharedPreferences

/**
 * Stores session data that persists after application close.
 *
 * To persist session data for different users, provide a different [prefs]
 * instance per user.
 */
class AppPersistentSession(
    private val prefs: SharedPreferences,
) {

    var walletId: String?
        get() = prefs.getString(WALLET_ID, null)
        set(value) = prefs.edit().putString(WALLET_ID, value).apply()

    var authentication: Boolean
        get() = prefs.getBoolean(AUTHENTICATION, false)
        set(value) = prefs.edit().putBoolean(AUTHENTICATION, value).apply()

    var encryptedBiometricUuid: String?
        get() = prefs.getString(ENCRYPTED_BIOMETRICS_UUID, null)
        set(value) = prefs.edit().putString(ENCRYPTED_BIOMETRICS_UUID, value).apply()

    var fcmToken: String?
        get() = prefs.getString(FCM_TOKEN, null)
        set(value) = prefs.edit().putString(FCM_TOKEN, value).apply()

    var hmsToken: String?
        get() = prefs.getString(HMS_TOKEN, null)
        set(value) = prefs.edit().putString(HMS_TOKEN, value).apply()

    var permissionCameraCheck: Boolean
        get() = prefs.getBoolean(PERMISSION_CAMERA_CHECK, false)
        set(value) = prefs.edit().putBoolean(PERMISSION_CAMERA_CHECK, value).apply()

    var createCredential: Boolean
        get() = prefs.getBoolean(CREATE_CREDENTIAL, false)
        set(value) = prefs.edit().putBoolean(CREATE_CREDENTIAL, value).apply()

    var register: Boolean
        get() = prefs.getBoolean(REGISTER, false)
        set(value) = prefs.edit().putBoolean(REGISTER, value).apply()

    var maskEmail: Boolean
        get() = prefs.getBoolean(EMAIL, false)
        set(value) = prefs.edit().putBoolean(EMAIL, value).apply()

    var maskCid: Boolean
        get() = prefs.getBoolean(CID, false)
        set(value) = prefs.edit().putBoolean(CID, value).apply()

    var maskMobile: Boolean
        get() = prefs.getBoolean(MOBILE, false)
        set(value) = prefs.edit().putBoolean(MOBILE, value).apply()

    var biometricFirstState: Boolean
        get() = prefs.getBoolean(BIOMETRIC_FIRST_STATE, false)
        set(value) = prefs.edit().putBoolean(BIOMETRIC_FIRST_STATE, value).apply()

    var gender: String?
        get() = prefs.getString(GENDER, null)
        set(value) = prefs.edit().putString(GENDER, value).apply()

    var birthDate: String?
        get() = prefs.getString(BIRTH_DATE, null)
        set(value) = prefs.edit().putString(BIRTH_DATE, value).apply()

    fun clear() {
        prefs.edit().clear().apply()
    }

    companion object {
        private const val WALLET_ID = "WALLET_ID"
        private const val AUTHENTICATION = "AUTHENTICATION"
        private const val ENCRYPTED_BIOMETRICS_UUID = "ENCRYPTED_BIOMETRICS_UUID"
        private const val FCM_TOKEN = "FCM_TOKEN"
        private const val HMS_TOKEN = "HMS_TOKEN"
        private const val PERMISSION_CAMERA_CHECK = "PERMISSION_CAMERA_CHECK"
        private const val CREATE_CREDENTIAL = "CREATE_CREDENTIAL"
        private const val REGISTER = "REGISTER"
        private const val EMAIL = "EMAIL"
        private const val CID = "CID"
        private const val MOBILE = "MOBILE"
        private const val BIOMETRIC_FIRST_STATE = "BIOMETRIC_FIRST_STATE"
        private const val GENDER = "GENDER"
        private const val BIRTH_DATE = "BIRTH_DATE"
    }
}
