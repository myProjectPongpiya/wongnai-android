package com.wongnai.appfoundation.data

import com.wongnai.appfoundation.i18n.models.SupportedLanguage

class CommonContext {
  var appLanguage: SupportedLanguage = SupportedLanguage.THAI
}
