package com.wongnai.appfoundation.data

import android.content.SharedPreferences
import java.util.UUID

/**
 * [DeviceIdStore] generates and persists a Device ID token for as long as
 * the application is installed.
 */
class DeviceIdStore constructor(
    private val deviceIdSharedPreferences: SharedPreferences
) {

    fun getDeviceId(): String? {
        return deviceIdSharedPreferences.getString(DATA_KEY, null)
    }

    fun setDeviceId(deviceId: String) {
        deviceIdSharedPreferences.edit()
            .putString(DATA_KEY, deviceId)
            .apply()
    }

    fun generate() = "${UUID.randomUUID()}-devc"

    companion object {
        private const val DATA_KEY = "device_id"
    }
}
