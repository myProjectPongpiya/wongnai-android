package com.wongnai.appfoundation.dialog

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.extensions.goneIf
import com.wongnai.appfoundation.extensions.inflateWithTheme
import com.wongnai.appfoundation.i18n.T
import com.wongnai.appfoundation.text.CustomSpannable
import kotlinx.android.synthetic.main.layout_bottom_dialog.*

/**
 * Dialog will display a simple bottom menu with title and a ticker
 * title -> title of option
 * ticker -> mark user selection
 */

class DrBottomDialog(
    private val heading: String? = null,
    disableDrag: Boolean = false,
    forceFullHeight: Boolean = false
) : DrBottomSheetDialogFragment(
    disableDrag = disableDrag,
    forceFullHeight = forceFullHeight,
    bottomSheetHeightPercentage = MAX_DIALOG_HEIGHT
) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflateWithTheme(context, R.layout.layout_bottom_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val message = CustomSpannable.buildString {

        }
        tvHeader.goneIf { heading.isNullOrEmpty() }
        tvHeader.text = heading
        ivIcClose.setOnClickListener {
            dismiss()
        }
        tvWarn.text = context?.let {
            message.toAndroidSpannable(it) {
                tvWarn.movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    companion object {
        private const val MAX_DIALOG_HEIGHT = 70
        private val GREEN_BOLD_TITLE_STYLE =
            CustomSpannable.Style.TextAppearance(R.style.DrTextAppearance_Green_Bold)
    }
}