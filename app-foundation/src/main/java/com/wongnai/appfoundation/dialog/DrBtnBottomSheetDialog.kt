package com.wongnai.appfoundation.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.wongnai.appfoundation.R
import kotlinx.android.synthetic.main.layout_btn_bottomsheet.view.*

class DrBtnBottomSheetDialog : DrBottomSheetDialogFragment(disableDrag = true) {

    private var title: String? = null
    private var listener: (() -> Unit)? = null

    override fun getTheme(): Int = R.style.DrBottomSheetDialog

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), theme)
            .apply {
                setOnShowListener {
                    val bottomSheet = findViewById<FrameLayout>(R.id.design_bottom_sheet)!!
                    BottomSheetBehavior.from(bottomSheet).run {
                        peekHeight = context.resources.getDimension(R.dimen.spacing_bottom_sheet_peek_height).toInt()
                    }
                }
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_btn_bottomsheet, container, false).apply {
            title?.let { btnDialog.text = it }
            btnDialog.setOnClickListener {
                listener?.invoke()
            }
        }
    }

    companion object {
        fun newInstance(
            title: String? = null,
            listener: () -> Unit
        ) =
            DrBtnBottomSheetDialog().apply {
                this.title = title
                this.listener = listener
            }
    }
}
