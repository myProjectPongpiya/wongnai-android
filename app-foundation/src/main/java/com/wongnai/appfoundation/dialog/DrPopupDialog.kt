package com.wongnai.appfoundation.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.extensions.inflateWithTheme
import com.wongnai.appfoundation.text.CustomSpannable
import kotlinx.android.synthetic.main.layout_custom_popup.*
import timber.log.Timber

/**
 * A reusable alert dialog that appears from the bottom of the screen. The
 * dialog's title, message, hero image can be configured via the arguments
 * bundle.
 *
 * You may optionally setDeviceId a callback to be invoked when the dialog is dismissed
 * using [setOnDismissListener].
 */
class DrPopupDialog : DrDialogFragment(disableClickOutside = true) {

    private var onDismissListener: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflateWithTheme(context, R.layout.layout_custom_popup, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        ivDialog.bindValue(arguments?.getInt(ARGS_HERO_IMAGE))
        tvRefId.bindValue(arguments?.getString(ARGS_TITLE))
        val customSpannable = arguments?.getSerializable(ARGS_SUB_MESSAGE) as? CustomSpannable
        tvMessage.bindValue(arguments?.getString(ARGS_MESSAGE), customSpannable)
        tvMessage.movementMethod = LinkMovementMethod.getInstance()

        arguments?.getString(ARGS_BUTTON_TEXT)?.let { text ->
            btnPrimary.text = text
        }
        btnPrimary.setOnClickListener(this::onPrimaryButtonClick)
    }

    private fun TextView.bindValue(value: String?, subValue: CustomSpannable? = null) {
        isVisible = !value.isNullOrBlank() || !subValue.isNullOrBlank()
        text = if (!subValue.isNullOrBlank()) subValue.toAndroidSpannable(context) else value
    }

    private fun ImageView.bindValue(value: Int?) {
        isGone = value != null || value != 0
        value?.let { setImageResource(it) }
    }

    /**
     * Set a [listener] to be invoked when the dialog is dismissed.
     */
    fun setOnDismissListener(listener: () -> Unit) {
        onDismissListener = listener
    }

    private fun onPrimaryButtonClick(v: View) {
        this.dismiss()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            val ft: FragmentTransaction = manager.beginTransaction()
            ft.add(this, tag)
            ft.commit()
        } catch (e: IllegalStateException) {
            Timber.e("Exception: %s", e.message)
        }
    }

    companion object {

        /**
         * Returns a new instance of [DrPopupDialog].
         */
        fun newInstance(
            title: String,
            message: String,
            subMessage: CustomSpannable? = null,
            buttonText: String,
            @DrawableRes heroImage: Int? = null,
            onDismiss: (() -> Unit)? = null,
        ): DrPopupDialog {
            val bundle = Bundle().apply {
                putString(ARGS_TITLE, title)
                putString(ARGS_MESSAGE, message)
                putSerializable(ARGS_SUB_MESSAGE, subMessage)
                putString(ARGS_BUTTON_TEXT, buttonText)

                heroImage?.let { putInt(ARGS_HERO_IMAGE, it) }
            }

            val dialog = DrPopupDialog().apply {
                arguments = bundle
            }

            if (onDismiss !== null) {
                dialog.setOnDismissListener(onDismiss)
            }

            return dialog
        }

        private const val ARGS_TITLE = "ARGS_TITLE"
        private const val ARGS_MESSAGE = "ARGS_MESSAGE"
        private const val ARGS_SUB_MESSAGE = "ARGS_SUB_MESSAGE"
        private const val ARGS_BUTTON_TEXT = "ARGS_BUTTON_TEXT"
        private const val ARGS_HERO_IMAGE = "ARGS_DIALOG_IMAGE"
    }
}
