package com.wongnai.appfoundation.enums

import com.google.gson.annotations.SerializedName

enum class NotificationType {
    @SerializedName("ANNOUNCE")
    ANNOUNCE,

    NULL
}