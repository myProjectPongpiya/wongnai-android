package com.wongnai.appfoundation.enums

enum class RequestMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}