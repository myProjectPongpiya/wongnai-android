package com.wongnai.appfoundation.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import com.wongnai.appfoundation.forms.DrTextInput

fun DrTextInput.showSoftKeyboard() {
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}