package com.wongnai.appfoundation.extensions

import com.wongnai.appfoundation.http.ApiException
import com.wongnai.appfoundation.http.ApiException.Companion.ERROR_CODE_403
import com.wongnai.appfoundation.http.interceptor.NoConnectivityException
import com.wongnai.appfoundation.ui.UIController
import com.wongnai.appfoundation.ui.showAlert
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import timber.log.Timber

/**
 * Toggles the loading state of [controller] when the observable is subscribed
 * to and after termination.
 */
fun <T> Flow<T>.doToggleLoadingStateOf(
    controller: UIController,
    vararg labels: String
): Flow<T> = this
    .onStart { controller.setLoading(true, *labels) }
    .onCompletion { controller.setLoading(false) }

/**
 * Terminal flow operator that launches the Flow in [scope], which defaults to
 * the provided [UIController].
 *
 * This applies common error handling, network error handling and etc.
 */
fun <T> Flow<T>.launchWith(
    controller: UIController,
    scope: CoroutineScope = controller,
    onError: ((Throwable) -> Unit)? = null,
) = this
    .retryWhen { cause, _ ->
        if (cause is NoConnectivityException) {
            showNoInternetAlert(controller)
            true
        } else {
            false
        }
    }.catch {
        Timber.d("$it")
        val apiException = it as? ApiException
        when {
            apiException?.response?.code() == ERROR_CODE_403 -> {
            }
            else -> onError?.let { _ ->
                onError.invoke(it)
            } ?: run {
                controller.showAlert(it)
            }
        }
    }
    .launchIn(scope)

/**
 * Terminal flow operator that collects the results of this Flow while applying
 * common behaviours using [controller].
 *
 * This applies common error handling, network error handling, and etc.
 */
suspend fun <T> Flow<T>.collectWith(
    controller: UIController,
    onEach: suspend (value: T) -> Unit
) = this
    .retryWhen { cause, _ ->
        if (cause is NoConnectivityException) {
            showNoInternetAlert(controller)
            true
        } else {
            false
        }
    }.catch {
        controller.showAlert(it)
    }
    .collect(onEach)

/**
 * Creates a flow that produces a single value from the return value of [action].
 */
fun <T> flowSingle(action: suspend () -> T): Flow<T> = flow { emit(action.invoke()) }

/**
 * Zips this Flow with the [other] Flow and ignores the emissions.
 */
fun <T1, T2> Flow<T1>.zip(other: Flow<T2>): Flow<Unit> = this.zip(other) { _, _ -> Unit }

/**
 * shows No internet alert dialog
 * This function is suspendable
 */
private suspend fun showNoInternetAlert(uiController: UIController) =
    suspendCancellableCoroutine<Unit> { continuation ->
        uiController.showNoConnectionError { continuation.resume(Unit) }
    }
