package com.wongnai.appfoundation.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.wongnai.appfoundation.graphics.DrImage
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * Loads an [image] into this ImageView.
 */
fun ImageView.setImage(image: DrImage) {
    Glide.with(this)
        .loadImage(image)
        .into(this)
}

fun ImageView.setImageWithCircleCrop(image: DrImage) {
    Glide.with(this)
        .loadImage(image)
        .circleCrop()
        .into(this)
}

suspend fun ImageView.setImageSuspended(image: DrImage) = suspendCoroutine<Unit> { continuation ->
    var isResumed = false
    Glide.with(this@setImageSuspended)
        .loadImage(image)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                if (!isResumed) {
                    isResumed = true
                    continuation.resume(Unit)
                }
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                this@setImageSuspended.setImageDrawable(resource)
                if (!isResumed) {
                    isResumed = true
                    continuation.resume(Unit)
                }
                return false
            }
        })
        .into(this@setImageSuspended)
}
