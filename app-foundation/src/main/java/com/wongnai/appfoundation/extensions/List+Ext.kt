package com.wongnai.appfoundation.extensions

import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.flatlist.DrFlatListItem
import com.wongnai.appfoundation.graphics.DrImage

/**
 * setting the tick icon for selected item
 */
fun List<DrFlatListItem>.withCheckedAt(id: String?): List<DrFlatListItem> {
    if (id == null) return this
    return this.map {
        if (it !is DrFlatListItem.Item) return@map it
        if (it.id != id) return@map it
        return@map it.copy(iconEnd = DrImage.ResId(R.drawable.ic_tick_correct))
    }
}

/**
 * setting the tick icon for selected item
 */
fun List<DrFlatListItem>.withCheckedAt(id: List<String>?): List<DrFlatListItem> {
    if (id.isNullOrEmpty()) return this
    return this.map {
        if (it !is DrFlatListItem.Item) return@map it
        return@map if (id.any { listId -> listId == it.id }) {
            it.copy(iconEnd = DrImage.ResId(R.drawable.ic_tick_correct))
        } else {
            it
        }
    }
}
