package com.wongnai.appfoundation.extensions

import org.threeten.bp.LocalTime

fun Long.toCountdownFormat() = with(LocalTime.ofSecondOfDay(this)) {
    String.format("%02d:%02d", minute, second)
}
