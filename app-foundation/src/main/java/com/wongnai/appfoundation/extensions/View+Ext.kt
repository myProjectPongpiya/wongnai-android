package com.wongnai.appfoundation.extensions

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.view.View
import android.view.accessibility.AccessibilityEvent
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.wongnai.appfoundation.R


/**
 * Change this view's visibility to [View.VISIBLE].
 */
fun View.visible() {
    this.visibility = View.VISIBLE
}

/**
 * Change this view's visibility to [View.GONE].
 */
fun View.gone() {
    this.visibility = View.GONE
}

/**
 * Change this view's visibility to [View.GONE] if [predicate] returns `true`,
 * otherwise defaults to [default].
 */
fun View.goneIf(default: Int = View.VISIBLE, predicate: () -> Boolean) {
    if (predicate.invoke()) {
        this.visibility = View.GONE
    } else {
        this.visibility = default
    }
}

/**
 * Change this view's visibility to [View.INVISIBLE].
 */
fun View.invisible() {
    this.visibility = View.INVISIBLE
}

/**
 * Change this view's visibility to [View.INVISIBLE] if [predicate] returns
 * `true`, otherwise defaults to [default].
 */
fun View.invisibleIf(default: Int = View.VISIBLE, predicate: () -> Boolean) {
    if (predicate.invoke()) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = default
    }
}

/**
 * Forces the focus to be on this view.
 */
fun View.forceRequestFocus() {
    val previousMode = this.isFocusableInTouchMode
    this.isFocusableInTouchMode = true
    this.requestFocus()
    this.isFocusableInTouchMode = previousMode
}

fun View.focusForAccessibility() {
    this.requestFocus()
    this.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED)
}

fun ViewPager2.autoScroll(interval: Long) {
    val handler = Handler()
    var scrollPosition = 0

    val runnable = object : Runnable {

        override fun run() {
            val count = adapter?.itemCount ?: 0
            setCurrentItem(scrollPosition++ % count, true)
            handler.postDelayed(this, interval)
        }
    }
    registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            scrollPosition = position + 1
        }
    })
    handler.post(runnable)
}

fun AppCompatImageView.loadCircle(any: Any) {
    Glide.with(this).load(any)
        .circleCrop()
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .into(this)
}

fun AppCompatImageView.loadUrl(
    url: String,
    @DrawableRes placeholderRes: Int? = R.drawable.ic_coin_error,
    @DrawableRes onErrorRes: Int? = R.drawable.ic_coin_error
) {
    val builder = Glide
        .with(this)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.ALL)

    placeholderRes?.run {
        builder.placeholder(placeholderRes)
    }
    onErrorRes?.run {
        builder.error(this)
    }

    builder.into(this)
}

fun AppCompatImageView.loadCircleUrl(
    url: GlideUrl,
    @DrawableRes placeholderRes: Int? = null,
    @DrawableRes onErrorRes: Int? = null
) {
    val builder = Glide
        .with(this)
        .load(url)
        .circleCrop()
        .diskCacheStrategy(DiskCacheStrategy.NONE)

    placeholderRes?.run {
        builder.placeholder(placeholderRes)
    }
    onErrorRes?.run {
        builder.error(this)
    }

    builder.into(this)
}

fun AppCompatImageView.loadRes(@DrawableRes drawableRes: Int) {
    Glide.with(this)
        .load(drawableRes)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .into(this)
}
