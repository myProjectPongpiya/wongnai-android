package com.wongnai.appfoundation.flatlist

import android.os.Build
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.extensions.goneIf
import com.wongnai.appfoundation.extensions.setImage
import com.wongnai.appfoundation.extensions.visible
import com.wongnai.appfoundation.flatlist.description.DescriptionAdapter
import kotlinx.android.synthetic.main.item_common_list_header.view.*
import kotlinx.android.synthetic.main.item_flat_list.view.*
import kotlinx.android.synthetic.main.item_progress_bar.view.*

internal class DrFlatListViewHolder(
    view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        itemView.clItemContainer?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        onClickListener.invoke(absoluteAdapterPosition)
    }

    /**
     * Sets [data] and [onClick] into this view.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun setData(data: DrFlatListItem) {
        when (data) {
            is DrFlatListItem.Header -> setHeaderData(data)
            is DrFlatListItem.Item -> setItemData(data)
            is DrFlatListItem.Load -> setLoading()
        }
    }

    /**
     * Sets the [data] for Header view
     */
    private fun setHeaderData(data: DrFlatListItem.Header) {
        itemView.tvHeader?.text = data.label
    }

    /**
     * Sets the [data] and [onClick] for AccountItem view
     */
    @RequiresApi(Build.VERSION_CODES.M)
    private fun setItemData(data: DrFlatListItem.Item) {
        itemView.tvItemTitle.goneIf { data.title == null }
        itemView.rvItemsDescription.goneIf { data.description == null }
        itemView.ivItemIcon.goneIf { data.icon == null }
        itemView.ivDrawableEnd.goneIf { data.iconEnd == null }

        if (data.isClickable) {
            val outValue = TypedValue()
            itemView.context.theme.resolveAttribute(R.attr.selectableItemBackground, outValue, true)
            itemView.clItemContainer.setBackgroundResource(outValue.resourceId)
        }

        data.title?.let {
            itemView.tvItemTitle.text = it.toAndroidSpannable(itemView.context) {
                itemView.tvItemTitle.movementMethod = LinkMovementMethod.getInstance()
            }
        }

        data.description?.let {
            val descriptionAdapter = DescriptionAdapter()
            with(itemView.rvItemsDescription) {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = descriptionAdapter
            }
            descriptionAdapter.setItems(it)
        }

        data.icon?.let {
            itemView.ivItemIcon.setImage(it)
        }

        data.iconEnd?.let {
            itemView.ivDrawableEnd.setImage(it)
        }
    }

    fun setTitlePadding(padding: Float) {
        itemView.tvItemTitle.apply {
            setPadding(
                paddingLeft,
                paddingTop,
                paddingRight,
                padding.toInt()
            )
        }
    }

    fun setHorizontalPadding(padding: Float) {
        itemView.clItemContainer?.apply {
            setPadding(
                padding.toInt(),
                paddingTop,
                padding.toInt(),
                paddingBottom
            )
        }
    }

    fun setItemPaddingBottom(padding: Float) {
        itemView.clItemContainer?.apply {
            setPadding(
                paddingLeft,
                paddingTop,
                paddingRight,
                padding.toInt(),
            )
        }
    }

    private fun setLoading() {
        itemView.progressList.visible()
    }
}
