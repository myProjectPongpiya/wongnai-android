package com.wongnai.appfoundation.forms

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ImageSpan
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputLayout
import com.wongnai.appfoundation.R

class DrTextInputLayout : TextInputLayout {

    private var isEmptyState: Boolean = true

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val state = super.onCreateDrawableState(extraSpace + 1)

        if (isEmptyState) {
            mergeDrawableStates(state, STATE_EMPTY)
        }

        return state
    }

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        super.addView(child, params)

        // If the child is an instance of EditText, bind its empty state to
        // this component's drawable state.
        (child as? EditText)?.let { editText ->
            setEmptyState(editText.text.isNullOrEmpty())
            editText.addTextChangedListener { setEmptyState(it.isNullOrEmpty()) }
        }
    }

    override fun setError(errorText: CharSequence?) {
        super.setError(errorText)
        removeCaptionPadding(com.google.android.material.R.id.textinput_error)
    }

    override fun setHelperText(helperText: CharSequence?) {
        // Override all helper text to display with an icon
        val textWithIcon = helperText?.let {
            SpannableString("   $helperText").apply {
                val icon = ContextCompat.getDrawable(context, R.drawable.ic_info_forgot) ?: return@apply
                val iconSize = context.resources.getDimension(HELPER_LABEL_ICON_SIZE).toInt()
                icon.setBounds(0, 0, iconSize, iconSize)
                setSpan(ImageSpan(icon, ImageSpan.ALIGN_CENTER), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }

        super.setHelperText(textWithIcon)
        removeCaptionPadding(com.google.android.material.R.id.textinput_helper_text)
    }

    private fun setEmptyState(isEmpty: Boolean) {
        isEmptyState = isEmpty
        refreshDrawableState()
    }

    private fun removeCaptionPadding(captionLabelId: Int) {
        // By default, error labels are padded by 32px on the left and right. This
        // is a hacky, but necessary fix to remove those padding.
        //
        // WARNING: Relies on internal implementation (internal res id) and may
        //          break upon future updates to material library.
        val tvCaption = this.findViewById<View>(captionLabelId)
        (tvCaption?.parent?.parent as? LinearLayout)?.let {
            if (it.paddingLeft != 0) {
                val topPadding = context.resources.getDimension(ERROR_LABEL_PADDING_TOP).toInt()
                it.setPadding(0, topPadding, 0, 0)
            }
        }
    }

    companion object {
        private val STATE_EMPTY = intArrayOf(R.attr.state_empty)
        private val ERROR_LABEL_PADDING_TOP = R.dimen.spacing_1
        private val HELPER_LABEL_ICON_SIZE = R.dimen.forms_helper_icon
    }
}
