package com.wongnai.appfoundation.forms

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.extensions.invisibleIf
import kotlinx.android.synthetic.main.layout_number_pad.view.*

class NumberPad @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attr, defStyle) {

    private var onInputListener: ((NumberPadInput) -> Unit)? = null

    init {
        View.inflate(context, R.layout.layout_number_pad, this)
        setupListeners()
    }

    private fun setupListeners() {
        btn1.setOnClickListener { onInputListener?.invoke(NumberPadInput.ONE) }
        btn2.setOnClickListener { onInputListener?.invoke(NumberPadInput.TWO) }
        btn3.setOnClickListener { onInputListener?.invoke(NumberPadInput.THREE) }
        btn4.setOnClickListener { onInputListener?.invoke(NumberPadInput.FOUR) }
        btn5.setOnClickListener { onInputListener?.invoke(NumberPadInput.FIVE) }
        btn6.setOnClickListener { onInputListener?.invoke(NumberPadInput.SIX) }
        btn7.setOnClickListener { onInputListener?.invoke(NumberPadInput.SEVEN) }
        btn8.setOnClickListener { onInputListener?.invoke(NumberPadInput.EIGHT) }
        btn9.setOnClickListener { onInputListener?.invoke(NumberPadInput.NINE) }
        btn0.setOnClickListener { onInputListener?.invoke(NumberPadInput.ZERO) }
        btnDelete.setOnClickListener { onInputListener?.invoke(NumberPadInput.DELETE) }
        divAction.setOnClickListener { onInputListener?.invoke(NumberPadInput.ADDITIONAL_ACTION) }
    }

    /**
     * Set a callback to be invoked when a number is clicked.
     */
    fun setOnInputListener(listener: (NumberPadInput) -> Unit) {
        this.onInputListener = listener
    }

    /**
     * Configure the addtional action button label. Set to `null` to hide it.
     */
    fun setAdditionalActionLabel(label: String?) {
        divAction.invisibleIf { label.isNullOrBlank() }
        btnAction.text = label
    }
}
