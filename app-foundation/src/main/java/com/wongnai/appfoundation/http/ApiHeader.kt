package com.wongnai.appfoundation.http

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.annotations.SerializedName

/**
 * Represents an error response from the API.
 */
data class ApiHeader(
    @SerializedName("status")
    val status: String? = null
) {

    companion object {
        fun fromJsonString(body: String?, gson: Gson): ApiHeader? {
            if (body == null) return null
            return try {
                gson.fromJson(body, ApiHeader::class.java)
            } catch (e: JsonSyntaxException) {
                null
            }
        }
    }
}
