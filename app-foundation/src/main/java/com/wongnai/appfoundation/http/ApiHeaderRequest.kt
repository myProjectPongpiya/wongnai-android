package com.wongnai.appfoundation.http

import com.google.gson.annotations.SerializedName

data class ApiHeaderRequest(
    @SerializedName("reqDtm")
    val reqDtm: String? = null,
    @SerializedName("appVersion")
    val appVersion: String? = null,
    @SerializedName("clientOS")
    val clientOS: String? = null,
    @SerializedName("appId")
    val appId: String? = null,
    @SerializedName("uuid")
    val uuid: String? = null
)
