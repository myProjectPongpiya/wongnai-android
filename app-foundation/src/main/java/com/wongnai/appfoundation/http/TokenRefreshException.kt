package com.wongnai.appfoundation.http

import java.io.IOException

sealed class TokenRefreshException : IOException() {

    object IsExpired : TokenRefreshException()
    data class IsApiException(val exception: ApiException) : TokenRefreshException()
    data class IsSystemError(val exception: Exception) : TokenRefreshException()
}
