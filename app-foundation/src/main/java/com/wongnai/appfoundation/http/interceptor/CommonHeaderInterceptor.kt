package com.wongnai.appfoundation.http.interceptor

import com.wongnai.appfoundation.data.DeviceIdStore
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.util.*

/**
 * The [CommonHeaderInterceptor] adds the common headers required in all
 * outgoing requests.
 */
class CommonHeaderInterceptor constructor(
    private val clientVersion: String,
    private val platform: String,
    private val deviceIdStore: DeviceIdStore,
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .appendXPlatform()
            .appendXClientVersion()
            .appendXCorrelationId()
            .appendXDeviceId()
            .appendXChannelId()
            .appendContentType()
            .appendContentLength(chain.request().body()?.contentLength())
            .build()

        return chain.proceed(newRequest)
    }

    private fun Request.Builder.appendXChannelId(): Request.Builder {
        return this.header("X-Channel-Id", "MB")
    }

    private fun Request.Builder.appendXPlatform(): Request.Builder {
        return this.header("X-Platform", platform)
    }

    private fun Request.Builder.appendXClientVersion(): Request.Builder {
        return this.header("X-Client-Version", clientVersion)
    }

    private fun Request.Builder.appendXCorrelationId(): Request.Builder {
        val uuid = UUID.randomUUID().toString()
        return this.header("X-Correlation-ID", uuid)
    }

    private fun Request.Builder.appendXDeviceId(): Request.Builder {
        val deviceId = deviceIdStore.getDeviceId() ?: deviceIdStore.generate()
            .also { deviceIdStore.setDeviceId(it) }
        return this.header("X-Device-ID", deviceId)
    }

    private fun Request.Builder.appendContentType(): Request.Builder {
        return this.header("Content-Type", "application/json")
    }

    private fun Request.Builder.appendContentLength(contentLength: Long?): Request.Builder {
        return this.header("Content-Length", contentLength.toString())
    }
}
