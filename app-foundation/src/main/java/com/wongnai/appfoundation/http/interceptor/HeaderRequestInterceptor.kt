package com.wongnai.appfoundation.http.interceptor

import com.google.gson.Gson
import com.wongnai.appfoundation.data.DeviceIdStore
import com.wongnai.appfoundation.enums.RequestMethod
import com.wongnai.appfoundation.http.ApiHeaderRequest
import com.wongnai.appfoundation.text.DateFormat
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer
import org.json.JSONArray
import org.json.JSONObject
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.io.IOException

/**
 * The [HeaderRequestInterceptor] adds the headers request required in all
 * outgoing requests.
 */
class HeaderRequestInterceptor constructor(
    private val gson: Gson,
    private val appId: String,
    private val clientVersion: String,
    private val platform: String,
    private val deviceIdStore: DeviceIdStore,
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var newRequest = chain.request()
        val requestBuilder = newRequest.newBuilder()
        val newBody = mutableMapOf<String, Any>("headerReq" to onCreate())
        val body = bodyToString(newRequest)
        if (!body.isNullOrEmpty()) {
            newBody["content"] = JSONObject(body).toMap()
        }
        val newRequestBody = RequestBody.create(
            MediaType.parse("application/json;charset=UTF-8"),
            gson.toJson(newBody).toString()
        )
        newRequest = setRequestMethod(newRequest, requestBuilder, newRequestBody).build()
        return chain.proceed(newRequest)
    }

    private fun setRequestMethod(
        newRequest: Request,
        requestBuilder: Request.Builder,
        newRequestBody: RequestBody
    ): Request.Builder {
        return when (newRequest.method()) {
            RequestMethod.POST.name -> requestBuilder.post(newRequestBody)
            RequestMethod.PUT.name -> requestBuilder.put(newRequestBody)
            RequestMethod.PATCH.name -> requestBuilder.patch(newRequestBody)
            RequestMethod.DELETE.name -> requestBuilder.delete(newRequestBody)
            else -> requestBuilder.get()
        }
    }

    private fun bodyToString(request: Request): String? {
        return try {
            val copy = request.newBuilder().build()
            val buffer = Buffer()
            copy.body()?.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            "did not work"
        }
    }

    private fun onCreate() =
        ApiHeaderRequest(
            reqDtm = getReqDtm(),
            appVersion = clientVersion,
            clientOS = platform,
            appId = appId,
            uuid = getDeviceId(),
        )

    private fun getDeviceId(): String {
        return deviceIdStore.getDeviceId() ?: deviceIdStore.generate()
            .also { deviceIdStore.setDeviceId(it) }
    }

    private fun getReqDtm() =
        ZonedDateTime.now()
            .format(DateTimeFormatter.ofPattern(DateFormat.DR_FULL_DATE_TIME_SECONDS_SLASH.format))

    private fun JSONObject.toMap(): Map<String, *> = keys().asSequence().associateWith {
        when (val value = this[it]) {
            is JSONArray -> {
                val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
                JSONObject(map).toMap().values.toList()
            }
            is JSONObject -> value.toMap()
            JSONObject.NULL -> null
            else -> value
        }
    }
}