package com.wongnai.appfoundation.http.interceptor

import java.io.IOException

class NoConnectivityException : IOException()
