package com.wongnai.appfoundation.http.interceptor

import com.wongnai.appfoundation.authentication.Endpoint
import com.wongnai.appfoundation.authentication.TokenService
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import retrofit2.Invocation

/**
 * [TokenInjectorInterceptor] injects access tokens to outgoing requests.
 *
 * You must supply it with an implementation of [TokenService] for it to
 * retrieve the appropriate tokens.
 */
class TokenInjectorInterceptor(
    private val tokenService: TokenService,
    private val oAuthToken: String
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        if (request.requiresOauth()) {
            val newRequest = request.injectAuthorizationHeader("Basic $oAuthToken")
            return chain.proceed(newRequest)
        }

        val authToken = tokenService.getToken() ?: return chain.proceed(request)

        return chain.proceed(request.injectAuthorizationHeader("Bearer ${authToken.accessToken}"))
    }

    private fun Request.injectAuthorizationHeader(header: String) = newBuilder()
        .header("Authorization", header)
        .build()

    /**
     * Returns true if this endpoint is annotated with a requirement to send OAuth
     * credentials instead of an access token.
     */
    private fun Request.requiresOauth(): Boolean {
        val annotation = this.tag(Invocation::class.java)
            ?.method()
            ?.getAnnotation(Endpoint.RequiresOauth::class.java)
        return annotation != null
    }
}
