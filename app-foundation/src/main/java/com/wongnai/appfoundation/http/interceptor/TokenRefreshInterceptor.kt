package com.wongnai.appfoundation.http.interceptor

import com.google.gson.Gson
import com.wongnai.appfoundation.http.ApiException
import com.wongnai.appfoundation.http.ApiException.Companion.ERROR_CODE_PIN_BIOMETRIC_ACCESS_TOKEN_EXPIRED
import com.wongnai.appfoundation.http.ApiException.Companion.ERROR_CODE_PRELOGIN_ACCESS_TOKEN_EXPIRED
import com.wongnai.appfoundation.http.ApiException.Companion.ERROR_CODE_REFRESH_TOKEN_EXPIRED
import com.wongnai.appfoundation.http.ApiException.Companion.ERROR_CODE_TOKEN_EXPIRED
import com.wongnai.appfoundation.http.ApiHeader
import com.wongnai.appfoundation.http.TokenRefreshException
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

class TokenRefreshInterceptor(
    private val gson: Gson,
    private val tokenRefreshService: TokenRefreshService
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        val url = chain.request().url()
//        FirebaseCrashlytics.getInstance().log("token refresh expired url:$url")

        // Otherwise attempt to refresh the token and try again
        val newRequest = try {
            tokenRefreshService.refresh(request)
        } catch (e: Exception) {
            Timber.d("Exception: $e")
            throw
            when (e is ApiException) {
                true -> TokenRefreshException.IsApiException(e)
                else -> TokenRefreshException.IsSystemError(e)
            }
        }

        return chain.proceed(newRequest)
    }
}
