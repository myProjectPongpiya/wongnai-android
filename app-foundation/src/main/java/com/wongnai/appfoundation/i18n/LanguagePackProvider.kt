package com.wongnai.appfoundation.i18n

import com.i18next.android.Operation
import com.wongnai.appfoundation.i18n.models.SupportedLanguage

interface LanguagePackProvider {

  fun get(key: String, operation: Operation? = null): String?

  fun get(key: String, language: SupportedLanguage, operation: Operation? = null): String?

  fun setLanguage(language: SupportedLanguage)

  fun getLanguage(): SupportedLanguage

  fun initialize()
}
