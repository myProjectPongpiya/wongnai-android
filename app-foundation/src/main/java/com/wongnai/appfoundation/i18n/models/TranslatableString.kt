package com.wongnai.appfoundation.i18n.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.lang.NullPointerException

/**
 * Represents a [String] that is localizable
 */
@Parcelize
data class TranslatableString(
  val translations: Map<SupportedLanguage, String>
) : Serializable, Parcelable {

  constructor(vararg translations: Pair<SupportedLanguage, String>) : this(
    translations = translations.fold(mutableMapOf()) { acc, pair ->
      acc[pair.first] = pair.second
      return@fold acc
    }
  )

  /**
   * Returns the localized string for [language].
   *
   * @throws NullPointerException if not available
   */
  fun forLanguage(language: SupportedLanguage): String {
    return translations[language]
      ?: throw NullPointerException("No translation found for $language.")
  }

  /**
   * Returns the localized string for [language] or `null` if not available.
   */
  fun forLanguageOrNull(language: SupportedLanguage): String? {
    return translations[language]
  }
}
