package com.wongnai.appfoundation.lib.lifecycle

enum class BindDirection {
    ONE_WAY,
    TWO_WAY
}
