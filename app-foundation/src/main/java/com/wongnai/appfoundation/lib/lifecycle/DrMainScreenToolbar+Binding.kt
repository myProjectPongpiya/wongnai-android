package com.wongnai.appfoundation.lib.lifecycle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.wongnai.appfoundation.graphics.DrImage
import com.wongnai.appfoundation.toolbar.DrMainScreenToolbar

fun DrMainScreenToolbar.bindNotification(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<DrImage>
) = apply {
    liveData.observe(lifecycleOwner) { this.setNotificationImage(it) }
}