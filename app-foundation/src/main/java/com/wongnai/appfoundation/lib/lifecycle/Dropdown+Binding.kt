package com.wongnai.appfoundation.lib.lifecycle

import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.wongnai.appfoundation.flatlist.DrFlatListItem
import com.wongnai.appfoundation.forms.DrDropDown

/**
 * Bind the text of this [TextView] to this [liveData].
 */
fun DrDropDown.bindItems(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<List<DrFlatListItem>>
) = apply {
    liveData.observe(lifecycleOwner) { this.setItems(it) }
}

fun DrDropDown.bindText(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.setText(it) }
}

fun DrDropDown.bindHint(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.setButtonAndDialogTitle(it) }
}

fun DrDropDown.bindDefaultSelectedId(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>
) = apply {
    liveData.observe(lifecycleOwner) { this.setDefaultSelectedId(it) }
}

fun DrDropDown.bindDisableDropDown(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Boolean>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.isEnabled = !it
        this.setDisableBackground(!it)
    }
}
