package com.wongnai.appfoundation.lib.lifecycle

import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.wongnai.appfoundation.extensions.setImage
import com.wongnai.appfoundation.graphics.DrImage

/**
 * Binds the image resource of this [ImageView] to [liveData].
 */
fun ImageView.bindImageResource(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImageResource(it ?: 0)
    }
}

/**
 * Binds the [BNImage] of [liveData].
 */
fun ImageView.bindImage(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<DrImage>
) = apply {
    liveData.observe(lifecycleOwner) {
        this.setImage(it)
    }
}
