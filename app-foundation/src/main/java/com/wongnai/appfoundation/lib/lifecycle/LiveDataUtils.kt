package com.wongnai.appfoundation.lib.lifecycle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer

object LiveDataUtils {

    /**
     * Returns a [LiveData] mapped from the inputs of [sourceA] and [sourceB] using
     * [mapFunction].
     */
    fun <A, B, C> map(
        sourceA: LiveData<A>,
        sourceB: LiveData<B>,
        mapFunction: (A?, B?) -> C?
    ): LiveData<C> {
        val liveData = MediatorLiveData<C>()
        liveData.addSource(sourceA) { liveData.value = mapFunction(sourceA.value, sourceB.value) }
        liveData.addSource(sourceB) { liveData.value = mapFunction(sourceA.value, sourceB.value) }
        return liveData
    }

    /**
     * Returns a [LiveData] mapped from the inputs of [sourceA], [sourceB] and
     * [sourceC] using [mapFunction].
     */
    fun <A, B, C, D> map(
        sourceA: LiveData<A>,
        sourceB: LiveData<B>,
        sourceC: LiveData<C>,
        mapFunction: (A?, B?, C?) -> D?
    ): LiveData<D> {
        val liveData = MediatorLiveData<D>()
        liveData.addSource(sourceA) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value)
        }
        liveData.addSource(sourceB) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value)
        }
        liveData.addSource(sourceC) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value)
        }
        return liveData
    }

    /**
     * Returns a [LiveData] mapped from the inputs of [sourceA], [sourceB] and
     * [sourceC] using [mapFunction].
     */
    fun <A, B, C, D, E> map(
        sourceA: LiveData<A>,
        sourceB: LiveData<B>,
        sourceC: LiveData<C>,
        sourceD: LiveData<D>,
        mapFunction: (A?, B?, C?, D?) -> E?
    ): LiveData<E> {
        val liveData = MediatorLiveData<E>()
        liveData.addSource(sourceA) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value)
        }
        liveData.addSource(sourceB) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value)
        }
        liveData.addSource(sourceC) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value)
        }
        liveData.addSource(sourceD) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value)
        }
        return liveData
    }

    /**
     * Returns a [LiveData] mapped from the inputs of [sourceA], [sourceB],
     * [sourceC], [sourceD] and [sourceE] using [mapFunction].
     */
    fun <A, B, C, D, E, F> map(
        sourceA: LiveData<A>,
        sourceB: LiveData<B>,
        sourceC: LiveData<C>,
        sourceD: LiveData<D>,
        sourceE: LiveData<E>,
        mapFunction: (A?, B?, C?, D?, E?) -> F?
    ): LiveData<F> {
        val liveData = MediatorLiveData<F>()
        liveData.addSource(sourceA) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value, sourceE.value)
        }
        liveData.addSource(sourceB) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value, sourceE.value)
        }
        liveData.addSource(sourceC) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value, sourceE.value)
        }
        liveData.addSource(sourceD) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value, sourceE.value)
        }
        liveData.addSource(sourceE) {
            liveData.value = mapFunction(sourceA.value, sourceB.value, sourceC.value, sourceD.value, sourceE.value)
        }
        return liveData
    }

    /**
     * Returns a [LiveData] that aggregates the values of [sources], behaving like
     * an OR operator. Meaning if any of [sources] are `true`, then the resulting
     * value is also `true`.
     */
    fun or(vararg sources: LiveData<Boolean>): LiveData<Boolean> {
        val liveData = MediatorLiveData<Boolean>()
        val orFunction = Observer<Boolean> { liveData.value = sources.any { it.value == true } }
        sources.forEach { liveData.addSource(it, orFunction) }
        return liveData
    }

    fun and(vararg sources: LiveData<Boolean>): LiveData<Boolean> {
        val liveData = MediatorLiveData<Boolean>()
        val andFunction = Observer<Boolean> { liveData.value = sources.all { it.value == true } }
        sources.forEach { liveData.addSource(it, andFunction) }
        return liveData
    }
}
