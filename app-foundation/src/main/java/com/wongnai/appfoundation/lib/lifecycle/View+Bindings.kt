package com.wongnai.appfoundation.lib.lifecycle

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Binds the visibility of this [View] to this [liveData]. Sets the visibility
 * of this [View] to [visibilityIfFalse]. Defaults to [View.GONE].
 */
fun View.bindVisibility(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Boolean>,
    visibilityIfFalse: Int = View.GONE
) = apply {
    liveData.observe(lifecycleOwner) {
        this.visibility = if (it) View.VISIBLE else visibilityIfFalse
    }
}

/**
 * Binds the visibility of this [View] to this [liveData]. If the value of the
 * String value is null or empty, hide the view by setting its visibility to
 * [visibilityIfFalse]. Defaults to [View.GONE].
 */
@JvmName("bindVisibilityWithString")
fun View.bindVisibility(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>,
    visibilityIfFalse: Int = View.GONE
) = apply {
    liveData.observe(lifecycleOwner) {
        this.visibility = if (it?.isNotEmpty() == true) View.VISIBLE else visibilityIfFalse
    }
}

/**
 * Binds the enabled state of this [View] to [liveData].
 */
fun View.bindEnabled(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Boolean>,
) = apply {
    liveData.observe(lifecycleOwner) { this.isEnabled = it == true }
}

/**
 * Binds the clickable state of this [View] to [liveData].
 */
fun View.bindClickable(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Boolean>,
) = apply {
    liveData.observe(lifecycleOwner) { this.isClickable = it == true }
}

/**
 * Binds the background resource ID of this [View] to [liveData].
 */
fun View.bindBackgroundResource(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<Int>,
) = apply {
    liveData.observe(lifecycleOwner) { this.setBackgroundResource(it ?: 0) }
}

/**
 * Binds the content description of this [View] to [liveData].
 */
fun View.bindContentDescription(
    lifecycleOwner: LifecycleOwner,
    liveData: LiveData<String>,
) = apply {
    liveData.observe(lifecycleOwner) { this.contentDescription = it }
}

fun View.blink(
    times: Int = Animation.INFINITE,
    duration: Long = 100L,
    offset: Long = 20L,
    minAlpha: Float = 0.0f,
    maxAlpha: Float = 1.0f,
    repeatMode: Int = Animation.REVERSE
) {
    startAnimation(
        AlphaAnimation(minAlpha, maxAlpha).also {
            it.duration = duration
            it.startOffset = offset
            it.repeatMode = repeatMode
            it.repeatCount = times
        }
    )
}
