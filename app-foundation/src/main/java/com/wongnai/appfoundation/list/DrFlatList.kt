package com.wongnai.appfoundation.list

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.res.use
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.flatlist.DrFlatListAdapter
import com.wongnai.appfoundation.flatlist.DrFlatListDivider
import com.wongnai.appfoundation.flatlist.DrFlatListItem

/**
 * A [RecyclerView] that displays item list in flat view.
 */
class DrFlatList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
) : DrBaseInfinityScrollList(context, attrs, defStyle) {

    private var horizontalPadding = context.resources.getDimension(R.dimen.dimen_0)
    private var skipLastItemDivider = true
    private var disableItemDivider = false

    private val flatListAdapter by lazy {
        DrFlatListAdapter()
    }

    init {
        context.obtainStyledAttributes(attrs, R.styleable.DrFlatList).use { t ->
            t.getDimension(R.styleable.DrFlatList_titlePaddingButton, -1f).takeIf { it > -1 }?.let {
                flatListAdapter.setTitlePaddingButton(it)
            }
            t.getDimension(R.styleable.DrFlatList_horizontalPadding, -1f).takeIf { it > -1 }?.let {
                horizontalPadding = it
                flatListAdapter.setHorizontalPadding(horizontalPadding)
            }
            t.getDimension(R.styleable.DrFlatList_itemPaddingBottom, -1f).takeIf { it > -1 }?.let {
                flatListAdapter.setItemPaddingBottom(it)
            }
            t.getBoolean(R.styleable.DrFlatList_skipLastItemDivider, true).also {
                skipLastItemDivider = it
            }
            t.getBoolean(R.styleable.DrFlatList_disableItemDivider, false).also {
                disableItemDivider = it
            }
        }
    }

    init {
        super.initRecyclerView(
            adapter = flatListAdapter,
            layout = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false),
            decor = if (disableItemDivider) {
                null
            } else {
                DrFlatListDivider(
                    context = context,
                    horizontalPadding = horizontalPadding,
                    skipLastItem = skipLastItemDivider
                )
            }
        )
    }

    /**
     * Sets the [items] to be displayed.
     */
    fun setItems(items: List<DrFlatListItem>?) {
        flatListAdapter.setItems(items ?: emptyList())
    }

    /**
     * Load more [items] in this [RecyclerView].
     */
    fun loadMoreItems(items: List<DrFlatListItem>?) {
        flatListAdapter.loadMoreItems(items ?: emptyList())
    }

    /**
     * Clear the [items] to be displayed.
     */
    fun clearItems() {
        flatListAdapter.clearItems()
    }

    /**
     * Sets a [listener] to be invoked when an item is clicked.
     */
    fun setOnItemClickListener(listener: ((item: DrFlatListItem.Item) -> Unit)?) {
        flatListAdapter.setOnClickListener(listener)
    }

    fun showProgressBar() {
        flatListAdapter.showProgressBar()
    }

    fun hideProgressBar() {
        flatListAdapter.hideProgressBar()
    }

    fun getItems() = flatListAdapter.getItems()
}
