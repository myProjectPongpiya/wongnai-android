package com.wongnai.appfoundation.list.notificationlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.appfoundation.R

internal class NotificationAdapter : RecyclerView.Adapter<NotificationViewHolder>() {

    private var onClickListener: ((id: Int) -> Unit)? = null

    private var items = mutableListOf<NotificationItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val layoutRes = when (viewType) {
            ITEM_TYPE -> R.layout.item_notification
            LOAD_TYPE -> R.layout.item_progress_bar
            else -> throw IllegalStateException("Unknown viewType: $viewType")
        }
        val view = LayoutInflater
            .from(parent.context)
            .inflate(layoutRes, parent, false)
        return NotificationViewHolder(view, this::onClickViewHolder)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindData(items[position])
    }

    override fun getItemCount(): Int = items.size

    private fun onClickViewHolder(position: Int) {
        when (items[position]) {
            is NotificationItem.Item -> onClickListener?.invoke((items[position] as NotificationItem.Item).id)
            is NotificationItem.Load -> {
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is NotificationItem.Item -> ITEM_TYPE
        is NotificationItem.Load -> LOAD_TYPE
    }

    fun setItems(items: List<NotificationItem>) {
        if (items.isEmpty()) return
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnClickListener(listener: ((id: Int) -> Unit)?) {
        this.onClickListener = listener
    }

    fun showProgressBar() {
        val lastItemPosition = items.size - 1
        items.add(NotificationItem.Load())
        notifyItemInserted(lastItemPosition)
    }

    fun hideProgressBar() {
        val lastItemPosition = items.size - 1
        if (lastItemPosition != INVALID_POSITION &&
            items[lastItemPosition] is NotificationItem.Load
        ) {
            items.removeAt(lastItemPosition)
            notifyItemRemoved(lastItemPosition)
        }
    }

    companion object {
        const val ITEM_TYPE = 1
        const val LOAD_TYPE = 2
        const val INVALID_POSITION = -1
    }
}
