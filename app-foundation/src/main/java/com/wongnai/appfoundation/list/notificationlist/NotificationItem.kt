package com.wongnai.appfoundation.list.notificationlist

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.enums.NotificationType
import kotlinx.android.parcel.Parcelize

sealed class NotificationItem {

    @Parcelize
    data class Item(
        @SerializedName("id")
        val id: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("body")
        val body: String,
        @SerializedName("date")
        val date: String,
        @SerializedName("notificationType")
        val notificationType: NotificationType,
        @SerializedName("isShowButton")
        var isShowButton: Boolean = false,
    ) : Parcelable, NotificationItem() {

    fun getIconType(): Int {
        return when (notificationType) {
            NotificationType.ANNOUNCE -> {
                return R.drawable.ic_noti_announce_active
            }
            else -> R.drawable.bg_solid_circle
        }
    }
    }

    data class Load(
        val placeholder: String? = null
    ) : NotificationItem()
}