package com.wongnai.appfoundation.list.notificationlist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.appfoundation.extensions.visible
import kotlinx.android.synthetic.main.item_notification.view.ivNotificationType
import kotlinx.android.synthetic.main.item_notification.view.tvTitleNotification
import kotlinx.android.synthetic.main.item_notification.view.tvBodyNotification
import kotlinx.android.synthetic.main.item_notification.view.tvDateNotification
import kotlinx.android.synthetic.main.item_notification.view.btnNotification
import kotlinx.android.synthetic.main.item_progress_bar.view.*

internal class NotificationViewHolder(
    val view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    override fun onClick(v: View?) {
        onClickListener.invoke(adapterPosition)
    }

    fun bindData(item: NotificationItem) {
        when (item) {
            is NotificationItem.Item -> bindItem(item)
            is NotificationItem.Load -> setLoading()
        }
    }

    private fun bindItem(item: NotificationItem.Item) {
        itemView.apply {
            ivNotificationType.setImageResource(item.getIconType())
            tvTitleNotification.text = item.title
            tvBodyNotification.text = item.body
            tvDateNotification.text = item.date
//            btnNotification.isVisible = item.isShowButton
        }
        itemView.btnNotification.setOnClickListener(this)
        itemView.setOnClickListener(this)
    }

    private fun setLoading() {
        itemView.progressList.visible()
    }
}