package com.wongnai.appfoundation.menu

import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.dialog.DrBottomSheetDialogFragment
import com.wongnai.appfoundation.extensions.goneIf
import com.wongnai.appfoundation.extensions.inflateWithTheme
import com.wongnai.appfoundation.extensions.loadUrl
import com.wongnai.appfoundation.extensions.withCheckedAt
import com.wongnai.appfoundation.flatlist.DrFlatListItem
import kotlinx.android.synthetic.main.dialog_card_coin_bottom.*
import kotlinx.android.synthetic.main.layout_list_menu.*
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Dialog will display a simple bottom menu with title and a ticker
 * title -> title of option
 * ticker -> mark user selection
 */

class CardCoinBottomDialog(
    private val coinImage: String? = null,
    private val coinName: String? = null,
    private val coinNameColor: String? = null,
    private val coinNameABRV: String? = null,
    private val coiPrice: String? = null,
    private val coinMarketCap: String? = null,
    private val coinDetail: String? = null,
    private val coinWebUrl: String? = null,
    disableDrag: Boolean = false,
    forceFullHeight: Boolean = true,
    bottomSheetHeightPercentage: Int = MAX_DIALOG_HEIGHT,
) : DrBottomSheetDialogFragment(
    disableDrag = disableDrag,
    forceFullHeight = forceFullHeight,
    bottomSheetHeightPercentage = bottomSheetHeightPercentage
) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflateWithTheme(context, R.layout.dialog_card_coin_bottom, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        coinImage?.let { ivCoin.loadUrl(it) }
        tvNameCoin.apply {
            text = coinName
            coinNameColor?.let { this.setTextColor(Color.parseColor(it)) }
        }
        tvABRVCoin.text = getString(R.string.label_coin_name_abrv, coinNameABRV)
        tvPriceCoin.text = coiPrice?.toDouble()?.let { setDecimalFormat(it) }
        tvMarketCap.text = coinMarketCap?.toDouble()?.let { setDecimalFormat(it) }
        if (coinDetail.isNullOrEmpty()) {
            tvDetail.text = ""
        } else {
            tvDetail.text = Html.fromHtml(coinDetail)
        }
        tvGotoWebSite.setOnClickListener {
            coinWebUrl?.let { url -> onCallWebBrowser(url) }
        }
    }

    private fun setDecimalFormat(number: Double): String {
        val df = DecimalFormat("#.00000")
//        df.roundingMode = RoundingMode.CEILING
        val numberFormat = df.format(number).toString()
        return "$${numberFormat.toDouble()}"
    }

    companion object {
        private const val MAX_DIALOG_HEIGHT = 70
    }
}
