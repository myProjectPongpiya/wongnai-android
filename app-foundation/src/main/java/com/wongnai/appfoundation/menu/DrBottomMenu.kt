package com.wongnai.appfoundation.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.dialog.DrBottomSheetDialogFragment
import com.wongnai.appfoundation.extensions.goneIf
import com.wongnai.appfoundation.extensions.inflateWithTheme
import com.wongnai.appfoundation.extensions.withCheckedAt
import com.wongnai.appfoundation.flatlist.DrFlatListItem
import kotlinx.android.synthetic.main.layout_list_menu.*

/**
 * Dialog will display a simple bottom menu with title and a ticker
 * title -> title of option
 * ticker -> mark user selection
 */

class DrBottomMenu(
    private val heading: String? = null,
    private val items: List<DrFlatListItem>,
    private var selectedId: String? = null,
    private val onClickListener: ((item: DrFlatListItem.Item) -> Unit)? = null,
    disableDrag: Boolean = false,
    forceFullHeight: Boolean = false,
    bottomSheetHeightPercentage: Int = MAX_DIALOG_HEIGHT,
) : DrBottomSheetDialogFragment(
    disableDrag = disableDrag,
    forceFullHeight = forceFullHeight,
    bottomSheetHeightPercentage = bottomSheetHeightPercentage
) {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflateWithTheme(context, R.layout.layout_list_menu, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvHeader.goneIf { heading.isNullOrEmpty() }
        tvHeader.text = heading
        rvMenuList.setItems(items.withCheckedAt(selectedId))
        rvMenuList.setOnItemClickListener {
            setSelectedId(it)
            onClickListener?.invoke(it)
            dismiss()
        }
        ivIcClose.setOnClickListener {
            dismiss()
        }
    }

    private fun setSelectedId(item: DrFlatListItem.Item) {
        selectedId = item.id
        rvMenuList.setItems(items.withCheckedAt(selectedId))
    }

    companion object {
        private const val MAX_DIALOG_HEIGHT = 70
    }
}
