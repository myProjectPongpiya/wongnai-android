package com.wongnai.appfoundation.model

import io.reactivex.disposables.CompositeDisposable

interface Disposer {
    val disposeBag: CompositeDisposable
}
