package com.wongnai.appfoundation.model

data class LoadingState(
    val isLoading: Boolean,
    val labels: List<String> = listOf()
)
