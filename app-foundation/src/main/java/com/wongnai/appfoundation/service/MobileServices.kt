package com.wongnai.appfoundation.service

import android.content.Context
import com.huawei.hms.api.HuaweiApiAvailability

fun Context.isHmsAvailable(): Boolean =
    HuaweiApiAvailability
        .getInstance()
        .isHuaweiMobileServicesAvailable(this) ==
            com.huawei.hms.api.ConnectionResult.SUCCESS
