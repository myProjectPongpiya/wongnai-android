package com.wongnai.appfoundation.text

import android.text.Editable

class AutoPhoneFormatTextWatcher(
    private val listener: (AutoFormatType) -> Unit,
    private var formattedString: String = EMPTY_DATA
) : DrBaseTextWatcher {

    override fun afterTextChanged(editable: Editable) {
        if (editable.toString() != formattedString) {
            formattedString = when {
                editable.toString() == EMPTY_DATA -> editable.toString()
                !editable.toString().startsWith(PHONE_0) -> formattedString
                editable.toString().startsWith(PHONE_00) -> formattedString
                editable.toString().startsWith(PHONE_01) -> formattedString
                editable.toString().startsWith(PHONE_02) -> {
                    listener.invoke(AutoFormatType.PHONE_NO_IN_BKK)
                    FormatUtils.formatInputByType(
                        editable.toString(),
                        AutoFormatType.PHONE_NO_IN_BKK
                    )
                }
                editable.toString().startsWith(PHONE_06) || editable.toString()
                    .startsWith(PHONE_08) || editable.toString()
                    .startsWith(PHONE_09) -> {
                    listener.invoke(AutoFormatType.MOBILE_NO)
                    FormatUtils.formatInputByType(
                        editable.toString(),
                        AutoFormatType.MOBILE_NO
                    )
                }
                else -> {
                    listener.invoke(AutoFormatType.PHONE_NO)
                    FormatUtils.formatInputByType(
                        editable.toString(),
                        AutoFormatType.PHONE_NO
                    )
                }
            }
            editable.replace(0, editable.length, formattedString, 0, formattedString.length)
        }
    }

    companion object {
        const val EMPTY_DATA = ""
        const val PHONE_0 = "0"
        const val PHONE_00 = "00"
        const val PHONE_01 = "01"
        const val PHONE_02 = "02"
        const val PHONE_06 = "06"
        const val PHONE_08 = "08"
        const val PHONE_09 = "09"
    }
}