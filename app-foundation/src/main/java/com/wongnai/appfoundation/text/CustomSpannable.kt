package com.wongnai.appfoundation.text

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.TextAppearanceSpan
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.StyleRes
import androidx.core.content.ContextCompat
import com.wongnai.appfoundation.R
import java.io.Serializable

/**
 * [CustomSpannable] is a data class to contain information about a multi-styled
 * String content.
 *
 * This is an abstraction above Android's [Spannable] that is easier to use, and
 * more importantly, is unit testable.
 */
data class CustomSpannable(
    val str: String,
    val styles: List<Pair<IntRange, Style>> = listOf()
) : CharSequence, Comparable<String>, Serializable {

    override val length: Int
        get() = str.length

    override fun get(index: Int): Char {
        return str[index]
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        return str.subSequence(startIndex, endIndex)
    }

    override fun compareTo(other: String): Int {
        return str.compareTo(other)
    }

    override fun toString(): String {
        return str
    }

    /**
     * Returns this [CustomSpannable] as its Android [Spannable] equivalent.
     */
    fun toAndroidSpannable(context: Context, listener: (() -> Unit)? = null): Spannable {
        val spannable = SpannableString(str)

        styles.forEach {
            val range = it.first

            val spansToApply = when (val style = it.second) {
                is Style.Default -> null
                is Style.Bolded -> listOf(
                    StyleSpan(Typeface.BOLD)
                )
                is Style.Colored -> listOf(
                    ForegroundColorSpan(ContextCompat.getColor(context, style.color))
                )
                is Style.TextAppearance -> {
                    listener?.invoke()
                    listOf(
                        TextAppearanceSpan(context, style.textAppearance)
                    )
                }
                is Style.Clickable -> listOf(
                    createClickableAndroidSpan(context, style)
                )
                is Style.LineHeight -> listOf(
                    DrLineHeightSpan(style.pxHeight)
                )
            } ?: return@forEach

            spansToApply.forEach { span ->
                spannable.setSpan(span, range.first, range.last, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
            }
        }

        return spannable
    }

    private fun createClickableAndroidSpan(context: Context, style: Style.Clickable) =
        object : ClickableSpan() {
            override fun onClick(v: View) {
                style.onClick.invoke()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = style.isShowUnderline
                ds.linkColor = context.resources.getColor(R.color.color_primary)
                if (style.isShowUnderline) super.updateDrawState(ds)
            }
        }

    class Builder {

        private var str = ""
        private var styles: MutableList<Pair<IntRange, Style>> = mutableListOf()
        private var globalStyles: MutableList<Style> = mutableListOf()

        /**
         * Append [s] into the string builder with [styles].
         */
        fun append(s: String, vararg styles: Style) = apply {
            val startIndex = str.length
            val endIndex = str.length + s.length
            styles.forEach { this.styles.add(IntRange(startIndex, endIndex) to it) }
            str += s
        }

        /**
         * Append the resulting value of [strFn] into the string builder with [style].
         */
        fun append(style: Style = Style.Default, strFn: () -> String) {
            append(strFn.invoke(), style)
        }

        /**
         * Append [s] into the string builder with [textAppearanceRes].
         */
        fun append(s: String, @StyleRes textAppearanceRes: Int) = apply {
            append(s, Style.TextAppearance(textAppearanceRes))
        }

        /**
         * Applies [style] between [startIndex] and [endInclusive].
         */
        fun apply(style: Style, startIndex: Int, endInclusive: Int) = apply {
            this.styles.add(IntRange(startIndex, endInclusive) to style)
        }

        /**
         * Applies [style] to the entire String of this [CustomSpannable].
         */
        fun applyToAll(style: Style) = apply {
            this.globalStyles.add(style)
        }

        /**
         * Builds the resulting [CustomSpannable] using the values provided to this
         * [Builder].
         */
        fun build(): CustomSpannable {
            val stylesToApply = styles.toMutableList() // Make a copy
            globalStyles.forEach {
                stylesToApply.add(IntRange(0, str.length) to it)
            }
            return CustomSpannable(str, stylesToApply)
        }
    }

    sealed class Style {
        data class TextAppearance(@StyleRes val textAppearance: Int) : Style()
        data class Colored(@ColorRes val color: Int) : Style()
        data class Clickable(val onClick: () -> Unit, val isShowUnderline: Boolean = false) :
            Style()

        data class LineHeight(val pxHeight: Int) : Style()
        object Bolded : Style()
        object Default : Style()
    }

    companion object {
        fun buildString(action: Builder.() -> Unit): CustomSpannable {
            return Builder().apply(action).build()
        }
    }
}
