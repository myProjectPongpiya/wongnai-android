package com.wongnai.appfoundation.text

enum class DateFormat(val format: String) {
  // Date formats specific to KTB BizNext
  DR_DATE("dd MMM yyyy"),
  DR_DATE_SLASH("dd/MM/yyyy"),
  DR_DATE_DASH("dd-MM-yyyy"),
  DR_DATE_BIRTH("yyyyMMdd"),
  DR_DATE_DASH_REVERT("yyyy-MM-dd"),
  DR_DATE_REMOVE_DASH("dd MM yyyy"),
  DR_TIME("HH:mm"),
  DR_DATETIME("dd MMM yyyy - HH:mm"),
  DR_CALENDAR_DATE("dd MMMM yyyy"),
  DR_YEAR_MONTH_DAY("yyyy-MM-dd"),
  DR_FULL_DATE_TIME("EEEE, dd MMM yyyy - HH:mm"),
  DR_FULL_DATE_SHORT("EEE, dd MMM yyyy"),
  DR_FULL_DATE_LONG("EEEE, dd MMM yyyy"),
  DR_DAY_MONTH("dd MMM"),
  DR_FULL_DATE_TIME_SECONDS("yyyy-MM-dd HH:mm:ss"),
  DR_FULL_DATE_TIME_SECONDS_SLASH("yyyy-MM-dd HH:mm:ss"),
  DR_DATETIME_HOUR_IN_AM_PM("dd MMM yy, HH:mm"),
  DR_FULL_DATE_TIME_T_SECONDS("yyyy-MM-dd'T'HH:mm:ss"),
  DR_MONTH_YEAR("MMM yyyy"),
  DR_YEAR_MONTH("yyyy-MM")
}
