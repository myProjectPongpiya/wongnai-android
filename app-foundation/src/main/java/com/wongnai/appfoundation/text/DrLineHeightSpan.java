package com.wongnai.appfoundation.text;

import android.graphics.Paint;
import android.text.style.LineHeightSpan;

import androidx.annotation.IntRange;
import androidx.annotation.Px;

/**
 * A spannable to customise line height. Adapted from the default
 * {@link Standard} which is only available from API Level 29.
 */
class DrLineHeightSpan implements LineHeightSpan {

  private final @Px
  int mHeight;

  /**
   * Set the line height of the paragraph to <code>height</code> physical pixels.
   */
  public DrLineHeightSpan(@Px @IntRange(from = 1) int height) {
    mHeight = height;
  }

  /**
   * Returns the line height specified by this span.
   */
  @Px
  public int getHeight() {
    return mHeight;
  }

  @Override
  public void chooseHeight(CharSequence text, int start, int end, int spanstartv, int lineHeight, Paint.FontMetricsInt fm) {
    final int originHeight = fm.descent - fm.ascent;
    // If original height is not positive, do nothing.
    if (originHeight <= 0) {
      return;
    }
    final float ratio = mHeight * 1.0f / originHeight;
    fm.descent = Math.round(fm.descent * ratio);
    fm.ascent = fm.descent - mHeight;
  }
}