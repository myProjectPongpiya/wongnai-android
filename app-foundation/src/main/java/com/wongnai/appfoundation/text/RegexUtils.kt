package com.wongnai.appfoundation.text

import java.util.regex.Pattern

object RegexUtils {

    fun hasSpecialCharacter(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.SPECIAL_CHARACTERS)
        val matcher = pattern.matcher(value)
        return hasEmoji(value) or matcher.matches()
    }

    private fun hasEmoji(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.EMOJI)
        val matcher = pattern.matcher(value)
        return matcher.find()
    }

    fun hasCharactersLimit(value: String, lowerLimit: Int, upperLimit: Int): Boolean {
        val charactersLimitPattern = "^(?=.*).{$lowerLimit,$upperLimit}$"
        val pattern = Pattern.compile(charactersLimitPattern)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun hasRepeatingDigits(value: String): Boolean {
        val pattern = Pattern.compile(PinRegex.REPEATING_DIGITS)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun hasNoSpace(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.NO_SPACE)
        val matcher = pattern.matcher(value)
        return !matcher.matches()
    }

    fun hasNoBlankSpace(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.NO_BLANK_SPACE)
        val matcher = pattern.matcher(value)
        return !matcher.matches()
    }

    fun hasMirroredDigits(value: String): Boolean {
        val pattern = Pattern.compile(PinRegex.MIRRORED_DIGITS)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun hasSequentialDigits(value: String): Boolean {
        val pattern = Pattern.compile(PinRegex.SEQUENTIAL_DIGIT)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun hasRepeatedSequentialDigits(value: String): Boolean {
        val pattern = Pattern.compile(PinRegex.REPEATED_SEQUENTIAL_DIGITS)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun hasThaiCharacters(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.THAI_CHARACTERS)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun isValidEmailId(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.EMAIL)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun isValidPhoneBKKNumber(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.PHONE_BKK_NUMBER)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun isValidPhoneNumber(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.PHONE_NUMBER)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun isValidMobileNumber(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.MOBILE_NUMBER)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }

    fun isValidMobileNumberWithoutSpecialCharacter(value: String): Boolean {
        val pattern = Pattern.compile(CommonRegex.MOBILE_NUMBER_WITHOUT_SPECIAL_CHARACTERS)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }
    fun isMinMaxDigitValid(value: String, regex: String): Boolean {
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(value)
        return matcher.matches()
    }
}
