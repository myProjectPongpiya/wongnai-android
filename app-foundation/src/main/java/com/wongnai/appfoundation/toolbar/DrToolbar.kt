package com.wongnai.appfoundation.toolbar

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.withStyledAttributes
import androidx.core.view.isGone
import androidx.core.widget.TextViewCompat
import com.wongnai.appfoundation.R
import com.wongnai.appfoundation.extensions.localizeIfPrefixed
import kotlinx.android.synthetic.main.layout_dr_toolbar.view.*

class DrToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyle, defStyleRes) {

    init {
        View.inflate(context, R.layout.layout_dr_toolbar, this)
        setStatusBarHeight()
        parseAttributes(attrs)

        if (this.isInEditMode) {
            this.setEditModePreview()
        }
    }

    private fun parseAttributes(attrs: AttributeSet?) {
        context.withStyledAttributes(attrs, R.styleable.DrToolbar) {
            val title = getString(R.styleable.DrToolbar_title)
            val color = getColor(R.styleable.DrToolbar_color, Color.parseColor(DEFAULT_TITLE_COLOR))
            title?.let { setTitleColor(color) }

            if (isInEditMode) {
                title?.let { setTitle(it) }
            } else {
                title?.let { setTitle(it.localizeIfPrefixed()) }
            }
        }
    }

    /**
     * Sets the title of [DrToolbar] to [title].
     */
    fun setTitle(title: String) {
        this.tvToolbarTitle.text = title
    }

    /**
     * Sets the title color of [DrToolbar] to [title].
     */
    fun setTitleColor(color: Int) {
        this.tvToolbarTitle.setTextColor(color)
        this.menuLeft.setTintColor(color)
    }

    /**
     * Render [items] as the left menu and register [onClick] as the callback
     * to be invoked when a menu item is clicked.
     */
    fun setLeftMenu(vararg items: DrToolbarMenuItem, onClick: (id: String) -> Unit) {
        this.menuLeft.setMenuItemClickListener(onClick)
        this.menuLeft.setMenuItems(items.toList(), DrToolbarMenu.Side.LEFT)
        setTitleMargins()
    }

    /**
     * Render [items] as the right menu and register [onClick] as the callback
     * to be invoked when a menu item is clicked.
     */
    fun setRightMenu(vararg items: DrToolbarMenuItem, onClick: (id: String) -> Unit) {
        this.menuRight.setMenuItemClickListener(onClick)
        this.menuRight.setMenuItems(items.toList(), DrToolbarMenu.Side.RIGHT)
        setTitleMargins()
    }

    /**
     * Convenient function to setDeviceId the left menu to a single icon button.
     * @see setLeftMenu
     */
    fun setLeftButton(@DrawableRes iconResId: Int, label: String? = null, onClick: () -> Unit) {
        val button = DrToolbarMenuItem.IconButton(
            id = DEFAULT_LEFT_BUTTON_ID,
            contentDescription = label ?: "",
            iconId = iconResId
        )
        setLeftMenu(button) { onClick.invoke() }
    }

    /**
     * Convenient function to setDeviceId the left menu to a single text button.
     * @see setLeftMenu
     */
    fun setLeftButton(label: String, onClick: () -> Unit) {
        val button = DrToolbarMenuItem.TextButton(
            id = DEFAULT_LEFT_BUTTON_ID,
            label = label
        )
        setLeftMenu(button) { onClick.invoke() }
    }

    fun disableRightMenu() {
        this.menuRight.changeState(false)
    }

    fun enableRightMenu() {
        this.menuRight.changeState(true)
    }

    /**
     * Convenient function to setDeviceId the right menu to a single icon button.
     * @see setRightMenu()
     */
    fun setRightButton(@DrawableRes iconResId: Int, label: String? = null, onClick: () -> Unit) {
        val button = DrToolbarMenuItem.IconButton(
            id = DEFAULT_RIGHT_BUTTON_ID,
            contentDescription = label ?: "",
            iconId = iconResId
        )
        setRightMenu(button) { onClick.invoke() }
    }

    /**
     * Convenient function to setDeviceId the right menu to a single text button.
     * @see setRightMenu()
     */
    fun setRightButton(label: String, onClick: () -> Unit) {
        val button = DrToolbarMenuItem.TextButton(
            id = DEFAULT_RIGHT_BUTTON_ID,
            label = label
        )
        setRightMenu(button) { onClick.invoke() }
    }

    fun hideLeft(isGone: Boolean = true) {
        this.menuLeft.isGone = isGone
        setTitleMargins()
    }

    fun hideRight(isGone: Boolean = true) {
        this.menuRight.isGone = isGone
        setTitleMargins()
    }

    private fun setLeftAlignedTitleStyle() {
        val constraints = ConstraintSet().apply {
            clone(this@DrToolbar.divToolbar)
            setHorizontalBias(R.id.tvToolbarTitle, 0f)
        }
        constraints.applyTo(this.divToolbar)
        this.menuLeft.visibility = View.GONE
        TextViewCompat.setTextAppearance(tvToolbarTitle, R.style.DrTextAppearance_ToolbarTitleLarge)
    }

    private fun setCenteredTitleStyle() {
        val constraints = ConstraintSet().apply {
            clone(this@DrToolbar.divToolbar)
            setHorizontalBias(R.id.tvToolbarTitle, 0.5f)
        }
        constraints.applyTo(this.divToolbar)

        this.menuLeft.visibility = View.VISIBLE
        TextViewCompat.setTextAppearance(tvToolbarTitle, R.style.DrTextAppearance_ToolbarTitle)
    }

    private fun setStatusBarHeight() {
        // We need these flags otherwise the WindowInsetListener will not be called
        systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        this.setOnApplyWindowInsetsListener { _, insets ->
            val statusBarHeight = insets.systemWindowInsetTop
            this.divStatusBar.layoutParams = LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                statusBarHeight
            )
            return@setOnApplyWindowInsetsListener insets.consumeSystemWindowInsets()
        }
    }

    private fun setTitleMargins() {
        if (isInEditMode) {
            return
        }

        post {
            (tvToolbarTitle.layoutParams as? ConstraintLayout.LayoutParams)?.let {
                val horizontalMargin = maxOf(menuLeft.width, menuRight.width)
                it.setMargins(horizontalMargin, 0, horizontalMargin, 0)
                tvToolbarTitle.layoutParams = it
            }
        }
    }

    private fun setEditModePreview() {
        setLeftButton(
            iconResId = R.drawable.ic_arrow_back,
            label = "Back"
        ) { /* do nothing */ }
        setRightButton("Cancel") { /* do nothing */ }
    }

    fun getRightIcon(): View {
        return menuRight
    }

    companion object {
        private const val DEFAULT_LEFT_BUTTON_ID = "DEFAULT_LEFT_BUTTON_ID"
        private const val DEFAULT_RIGHT_BUTTON_ID = "DEFAULT_RIGHT_BUTTON_ID"
        private const val DEFAULT_TITLE_COLOR = "#212121"
    }
}
