package com.wongnai.appfoundation.toolbar

import androidx.annotation.DrawableRes

sealed class DrToolbarMenuItem {
    abstract val id: String

    /**
     * A [DrToolbarMenuItem] that will be rendered as an icon button.
     *
     * @param id The [id] that will be passed to the menu's click listener. The
     *     rendered view will also be tagged with this [id] so you can query for it
     *     later using findViewWithTag.
     * @param contentDescription The [contentDescription] will be used as the icon's
     *   content description for accessibility purposes.
     * @param iconId The [DrawableRes] to render as the icon.
     */
    data class IconButton(
        override val id: String,
        val contentDescription: String,
        @DrawableRes val iconId: Int
    ) : DrToolbarMenuItem()

    /**
     * A [DrToolbarMenuItem] that will be rendered as a text button.
     *
     * @param id The [id] that will be passed to the menu's click listener. The
     *     rendered view will also be tagged with this [id] so you can query for it
     *     later using findViewWithTag.
     * @param label The [label] to be rendered on the menu item.
     */
    data class TextButton(
        override val id: String,
        val label: String
    ) : DrToolbarMenuItem()
}
