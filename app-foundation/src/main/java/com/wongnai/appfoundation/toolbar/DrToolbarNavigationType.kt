package com.wongnai.appfoundation.toolbar

enum class DrToolbarNavigationType {
    BACK,
    CLOSE,
    BOTH
}
