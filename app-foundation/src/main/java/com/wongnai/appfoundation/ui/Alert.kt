package com.wongnai.appfoundation.ui

import androidx.annotation.DrawableRes
import com.wongnai.appfoundation.text.CustomSpannable

/**
 * Represents an alert that should be displayed to the user.
 */
sealed class Alert {

    data class Popup(
        val title: String? = null,
        val message: String? = null,
        val subMessage: CustomSpannable? = null,
        val buttonLabel: String? = null,
        @DrawableRes val heroImage: Int? = null,
        val onDismiss: (() -> Unit)? = null
    ) : Alert()

    data class PopupMultiButton(
        val title: String? = null,
        val message: String? = null,
        @DrawableRes val heroImage: Int? = null,
        val onDismiss: (() -> Unit)? = null,
        val negativeButtonLabel: String? = null,
        val negativeButtonListener: (() -> Unit)? = null,
        val positiveButtonLabel: String? = null,
        val positiveButtonListener: (() -> Unit)? = null,
        val type: MultiButtonType
    ) : Alert()

    enum class MultiButtonType {
        VERTICAL,
        HORIZONTAL
    }

    data class NativePopup(
        val title: String? = null,
        val message: String? = null,
        val positiveButtonLabel: String? = null,
        val positiveButtonListener: (() -> Unit)? = null,
        val negativeButtonLabel: String? = null,
        val negativeButtonListener: (() -> Unit)? = null,
    ) : Alert()

    /**
     * Represents a toast alert.
     */
    data class Toast(
        val message: String,
        val type: ToastType
    ) : Alert()

    enum class ToastType {
        SUCCESS,
        PENDING,
        ERROR
    }
}
