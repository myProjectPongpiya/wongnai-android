package com.wongnai.apptestutils.data

import com.wongnai.appfoundation.http.ApiException
import com.wongnai.appfoundation.http.ApiHeader

object ApiExceptionMocks {

    /**
     * Returns an [ApiException] with the provided error [code].
     */
    fun withCode(code: String): ApiException {
        val error = ApiHeader()
        return ApiException(error)
    }
}
