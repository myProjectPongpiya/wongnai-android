package com.wongnai.apptestutils.extensions

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.mockito.stubbing.OngoingStubbing

/**
 * Mocks this [Flow] stub to return [items].
 */
fun <T> OngoingStubbing<Flow<T>>.thenReturnFlow(
    vararg items: T
): OngoingStubbing<Flow<T>> {
    val obs = items
        .map { flowOf(it) }
        .drop(1)
        .toTypedArray()
    return this.thenReturn(flowOf(items.first()), *obs)
}

/**
 * Mocks this stub to throw an [exception] when the Flow is executed.
 */
fun <T> OngoingStubbing<Flow<T>>.thenReturnErrorFlow(
    exception: Throwable = IllegalStateException()
): OngoingStubbing<Flow<T>> {
    return this.thenReturn(flow { throw exception })
}
