package com.wongnai.apptestutils.rules

import com.i18next.android.Operation
import com.wongnai.appfoundation.i18n.LanguagePackProvider
import com.wongnai.appfoundation.i18n.T
import com.wongnai.appfoundation.i18n.models.SupportedLanguage
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class LanguagePackRule(
  val provider: LanguagePackProvider = createMockLanguagePackProvider()
) : TestRule {

  override fun apply(base: Statement, description: Description): Statement {
    return object : Statement() {
      override fun evaluate() {
        T.setProvider(provider)
        base.evaluate()
      }
    }
  }

  companion object {
    fun createMockLanguagePackProvider(
      language: SupportedLanguage = SupportedLanguage.ENGLISH
    ) = mock<LanguagePackProvider>(lenient = true) {
      on { getLanguage() } doReturn language
      on { get(any(), any<Operation>()) } doAnswer { it.arguments[0] as String? }
    }
  }
}
