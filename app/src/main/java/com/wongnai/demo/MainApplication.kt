package com.wongnai.demo

import android.app.Application
import com.wongnai.appfoundation.i18n.I18nLayoutInflaterInterceptor
import com.jakewharton.threetenabp.AndroidThreeTen
import com.wongnai.demo.di.appComponent
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(appComponent)
        }

        bootstrapLogging()
        bootstrapViewPump()
    }

    private fun bootstrapLogging() {
        Timber.plant(Timber.DebugTree())
    }

    /**
     * Bootstraps layout inflation interceptors.
     */
    private fun bootstrapViewPump() {
        val viewPump = ViewPump.builder()
            .addInterceptor(I18nLayoutInflaterInterceptor())
            .build()
        ViewPump.init(viewPump)
    }
}