package com.wongnai.demo.di

import com.wongnai.demo.di.module.coreModule
import com.wongnai.demo.di.module.networkModule
import com.wongnai.demo.di.module.repositoryModule
import com.wongnai.demo.di.module.textModule
import com.wongnai.demo.di.module.useCaseModule
import com.wongnai.demo.di.module.viewModelModule

val appComponent = listOf(
    coreModule, textModule, networkModule, repositoryModule, useCaseModule, viewModelModule
)