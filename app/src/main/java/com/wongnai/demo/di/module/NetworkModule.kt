package com.wongnai.demo.di.module

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.wongnai.appfoundation.crypto.AESManager
import com.wongnai.appfoundation.crypto.AESService
import com.wongnai.appfoundation.crypto.encryption.EncryptionProviderImp
import com.wongnai.appfoundation.crypto.encryption.EncryptionService
import com.wongnai.appfoundation.crypto.manager.KeyProviderImp
import com.wongnai.appfoundation.crypto.manager.KeyService
import com.wongnai.appfoundation.http.interceptor.CommonExceptionInterceptor
import com.wongnai.appfoundation.http.interceptor.HttpOverrideInterceptor
import com.wongnai.appfoundation.http.interceptor.NoConnectivityInterceptor
import com.wongnai.appfoundation.http.interceptor.CryptoClientInterceptor
import com.wongnai.appfoundation.http.interceptor.ExceptionWrapperInterceptor
import com.wongnai.appfoundation.trustmanager.UnsafeOkHttpClient
import com.wongnai.demo.BuildConfig
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.X509TrustManager

val networkModule = module {
    factory {
        provideHttpClient(
            androidContext(),
            get(),
            get(),
            get()
        )
    }

    factory { NoConnectivityInterceptor(get()) }
    factory { HttpOverrideInterceptor() }
    factory { CryptoClientInterceptor(get()) }
    factory { CommonExceptionInterceptor(get()) }

    single { provideRetrofit(get(), get()) }

    single<AESService> { AESManager() }
    single<KeyService> { KeyProviderImp(get(named("publicKeyPEM"))) }
    single<EncryptionService> { EncryptionProviderImp(get(), get()) }
}

fun provideHttpClient(
    context: Context,
    checkForInternetConnectivity: NoConnectivityInterceptor,
    overrideHttpSettings: HttpOverrideInterceptor,
    checkForExceptions: CommonExceptionInterceptor,
): OkHttpClient {
    val builder = OkHttpClient.Builder()
        .addInterceptor(checkForInternetConnectivity)
        .addInterceptor(overrideHttpSettings)
        .addInterceptor(ExceptionWrapperInterceptor())
        .addInterceptor(checkForExceptions)
        .addInterceptor(chuckerInterceptor(context))
        .callTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .connectTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .readTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)
        .writeTimeout(CONNECTION_TIMEOUT_SECOND, TimeUnit.SECONDS)

    if (BuildConfig.FLAVOR == "dev") {
        UnsafeOkHttpClient.getSSLSocketFactory()?.let {
            builder.sslSocketFactory(
                it,
                UnsafeOkHttpClient.getTrustManager()[0] as X509TrustManager
            )
        }
        builder.hostnameVerifier { _, _ -> true }
    }

    return builder.build()
}

fun provideRetrofit(httpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_API_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

private fun chuckerInterceptor(
    context: Context
): ChuckerInterceptor {
    return ChuckerInterceptor.Builder(context)
        .collector(ChuckerCollector(context))
        .maxContentLength(CHUCKER_MAX_CONTENT_LENGTH)
        .redactHeaders(emptySet())
        .alwaysReadResponseBody(false)
        .build()
}

private const val CONNECTION_TIMEOUT_SECOND = 30.toLong()
private const val CHUCKER_MAX_CONTENT_LENGTH = 250000L
