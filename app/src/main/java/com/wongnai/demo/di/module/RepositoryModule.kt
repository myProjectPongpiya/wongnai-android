package com.wongnai.demo.di.module

import com.wongnai.appdata.repository.api.coin.CoinApi
import org.koin.dsl.module
import retrofit2.Retrofit

val repositoryModule = module {
    single { provideCoinApi(get()) }
}

fun provideCoinApi(retrofit: Retrofit): CoinApi {
    return retrofit.create(CoinApi::class.java)
}