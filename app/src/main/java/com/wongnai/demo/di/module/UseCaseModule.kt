package com.wongnai.demo.di.module

import com.wongnai.appdomain.usecase.coin.GetCoinDetailUseCase
import com.wongnai.appdomain.usecase.coin.GetCoinUseCase
import com.wongnai.appdomain.usecase.coin.SearchUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetCoinUseCase(get()) }
    factory { GetCoinDetailUseCase(get()) }
    factory { SearchUseCase(get()) }
}