package com.wongnai.demo.di.module

import com.wongnai.demo.ui.listOfCoin.ListOfCoinViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ListOfCoinViewModel(get(), get(), get()) }
}