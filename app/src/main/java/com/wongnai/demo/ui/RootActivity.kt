package com.wongnai.demo.ui

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.navigation.Navigation.findNavController
import com.wongnai.appfoundation.base.BaseActivity
import com.wongnai.appfoundation.lib.eventbus.AppEvent
import com.wongnai.appfoundation.lib.eventbus.EventBus
import com.wongnai.demo.R
import com.wongnai.demo.databinding.ActivityRootBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class RootActivity : BaseActivity(), AppTimeoutHelper.Listener {

    private val binding: ActivityRootBinding by lazy { ActivityRootBinding.inflate(layoutInflater) }

    private val eventBus: EventBus by inject()

    private val appTimeoutHelper: AppTimeoutHelper by inject()

    private fun navController() = findNavController(this, R.id.nav_host_fragment)

    override fun onUserInteraction() {
        super.onUserInteraction()
        appTimeoutHelper.registerInteraction()
    }

    override fun onResume() {
        super.onResume()
        appTimeoutHelper.start()
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_SECURE,
//            WindowManager.LayoutParams.FLAG_SECURE
//        )
        binding.navHostFragment.isVisible = true
        binding.ivAppLogo.isGone = true
    }

    override fun onPause() {
        super.onPause()
//        window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
//        window?.setFlags(
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//        )
        binding.ivAppLogo.isVisible = true
        binding.navHostFragment.isGone = true
    }

    override fun onDestroy() {
        super.onDestroy()
        appTimeoutHelper.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupStatusBar()
        listenForEventBus()
        checkTheme()
        onSetThemeApp()
    }

    private fun checkTheme() {
        when (MyPreferences(this).darkMode) {
            0 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                delegate.applyDayNight()
            }
            1 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                delegate.applyDayNight()
            }
            2 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                delegate.applyDayNight()
            }
        }
    }

    private fun onSetThemeApp() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        MyPreferences(this).darkMode = 2
        delegate.applyDayNight()
    }

    class MyPreferences(context: Context?) {

        companion object {
            private const val DARK_STATUS = "io.github.manuelernesto.DARK_STATUS"
        }

        private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

        var darkMode = preferences.getInt(DARK_STATUS, 0)
            set(value) = preferences.edit().putInt(DARK_STATUS, value).apply()
    }

    private fun setupStatusBar() {
        window.apply {
            statusBarColor = getColor(android.R.color.background_dark)
            decorView.systemUiVisibility = View.STATUS_BAR_HIDDEN
        }
    }

    private fun listenForEventBus() {
        appTimeoutHelper.setListener(this@RootActivity)
        launch {
            eventBus.listen(AppEvent.LoginSuccess::class.java)
                .collect {
                    appTimeoutHelper.setLogin(true)
                    appTimeoutHelper.restartSessionTimeout()
                }
        }
    }

    override fun restartSession() {
        appTimeoutHelper.stop()
        navController().navigate(navController().graph.startDestination)
    }

    override fun onSessionTimeout() {
        restartSession()
    }
}