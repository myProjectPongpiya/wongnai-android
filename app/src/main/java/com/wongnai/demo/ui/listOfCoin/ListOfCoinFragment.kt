package com.wongnai.demo.ui.listOfCoin

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.wongnai.appdata.repository.model.coin.CoinDetailResponseModel
import com.wongnai.appfoundation.base.ViewBindingFragment
import com.wongnai.appfoundation.extensions.bindViewModel
import com.wongnai.appfoundation.extensions.gone
import com.wongnai.appfoundation.extensions.loadUrl
import com.wongnai.appfoundation.extensions.removeDashes
import com.wongnai.appfoundation.extensions.visible
import com.wongnai.appfoundation.lib.lifecycle.withViewLifecycleOwner
import com.wongnai.appfoundation.menu.CardCoinBottomDialog
import com.wongnai.demo.R
import com.wongnai.demo.databinding.ScreenListOfCoinBinding
import com.wongnai.demo.ui.listOfCoin.coinCard.CoinCardItem
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ListOfCoinFragment :
    ViewBindingFragment<ScreenListOfCoinBinding>(R.layout.screen_list_of_coin) {

    private val vm by bindViewModel { getViewModel(clazz = ListOfCoinViewModel::class) }

    override fun initializeLayoutBinding(view: View) =
        ScreenListOfCoinBinding.bind(view)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.initialize()
        with(vm) {
            onSearchView()
            swipeRefresh()
            withViewLifecycleOwner {
                coinList.observe {
                    layout.wgtCoinCard.setItems(it)
                    if (it.size > 2) {
                        onSetTopRank3(it)
                    }
                }
                layout.wgtCoinCard.setOnItemClickListener { uuid ->
                    onGetCoinDetailUseCase(uuid)
                }
                coinDetailResponse.observe {
                    coinCardDialogBottomSheet(it)
                }
                isGetCoinResponseError.observe {
                    if (it == true) {
                        layout.swipeRefresh.gone()
                        layout.layoutLoadError.visible()
                        layout.layoutLoadEmpty.gone()
                    } else {
                        layout.swipeRefresh.visible()
                        layout.layoutLoadError.gone()
                        layout.layoutLoadEmpty.gone()
                    }
                }
                isGetCoinResponseEmpty.observe {
                    if (it == true) {
                        layout.layoutLoadEmpty.visible()
                        layout.swipeRefresh.gone()
                        layout.layoutLoadError.gone()
                    } else {
                        layout.layoutLoadEmpty.gone()
                        layout.swipeRefresh.visible()
                        layout.layoutLoadError.gone()
                    }
                }
                layout.tvTryAgain.setOnClickListener {
                    onClickTryAgain()
                }
            }
        }
    }

    private fun onSearchView() {
        with(vm) {
            withViewLifecycleOwner {
                layout.search.setOnQueryTextListener(object :
                    androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(keyword: String?): Boolean {
                        keyword?.let { onSearchUseCase(it) }
                        if (keyword.isNullOrEmpty()) {
                            startCooldown()
                        }
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        newText?.let { onSearchUseCase(it) }
                        layout.layoutTop3.gone()
                        if (newText.isNullOrEmpty()) {
                            startCooldown()
                            layout.layoutTop3.visible()
                        }
                        return false
                    }
                })
            }
        }
    }

    private fun swipeRefresh() {
        with(vm) {
            withViewLifecycleOwner {
                layout.swipeRefresh.setOnRefreshListener {
                    Handler().postDelayed({
                        onSwipeRefresh()
                        layout.swipeRefresh.isRefreshing = false
                    }, 2000)
                }
                layout.swipeRefresh.setColorSchemeColors(
                    Color.parseColor("#008744"),
                    Color.parseColor("#d62d20")
                )
            }
        }
    }

    private fun onSetTopRank3(it: List<CoinCardItem>) {
        it[0].imgCoin?.let { img -> layout.divTop3.imgCoinTop1.loadUrl(img) }
        it[1].imgCoin?.let { img -> layout.divTop3.imgCoinTop2.loadUrl(img) }
        it[2].imgCoin?.let { img -> layout.divTop3.imgCoinTop3.loadUrl(img) }
        layout.divTop3.tvNameCoinTop1.text = it[0].tvNameCoin
        layout.divTop3.tvABRVCoinTop1.text = it[0].tvABRVCoin
        layout.divTop3.tvCoinChangeTop1.text = it[0].change?.removeDashes() ?: ""
        it[0].change.let {
            if (it?.toDouble()!! > 0) {
                layout.divTop3.tvCoinChangeTop1.setTextColor(Color.GREEN)
                layout.divTop3.tvCoinChangeTop1.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_up,
                    0,
                    0,
                    0
                )
            } else {
                layout.divTop3.tvCoinChangeTop1.setTextColor(Color.RED)
                layout.divTop3.tvCoinChangeTop1.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_down,
                    0,
                    0,
                    0
                )
            }
        }
        layout.divTop3.tvNameCoinTop2.text = it[1].tvNameCoin
        layout.divTop3.tvABRVCoinTop2.text = it[1].tvABRVCoin
        layout.divTop3.tvCoinChangeTop2.text = it[1].change?.removeDashes() ?: ""
        it[1].change.let {
            if (it?.toDouble()!! > 0) {
                layout.divTop3.tvCoinChangeTop2.setTextColor(Color.GREEN)
                layout.divTop3.tvCoinChangeTop2.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_up,
                    0,
                    0,
                    0
                )
            } else {
                layout.divTop3.tvCoinChangeTop2.setTextColor(Color.RED)
                layout.divTop3.tvCoinChangeTop2.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_down,
                    0,
                    0,
                    0
                )
            }
        }
        layout.divTop3.tvNameCoinTop3.text = it[2].tvNameCoin
        layout.divTop3.tvABRVCoinTop3.text = it[2].tvABRVCoin
        layout.divTop3.tvCoinChangeTop3.text = it[2].change?.removeDashes() ?: ""
        it[2].change.let {
            if (it?.toDouble()!! > 0) {
                layout.divTop3.tvCoinChangeTop3.setTextColor(Color.GREEN)
                layout.divTop3.tvCoinChangeTop3.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_up,
                    0,
                    0,
                    0
                )
            } else {
                layout.divTop3.tvCoinChangeTop3.setTextColor(Color.RED)
                layout.divTop3.tvCoinChangeTop3.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_down,
                    0,
                    0,
                    0
                )
            }
        }
    }

    private fun coinCardDialogBottomSheet(response: CoinDetailResponseModel) {
        val dialog = CardCoinBottomDialog(
            coinImage = response.coin.iconURL,
            coinName = response.coin.name,
            coinNameColor = response.coin.color,
            coinNameABRV = response.coin.symbol,
            coiPrice = response.coin.price,
            coinMarketCap = response.coin.marketCap,
            coinDetail = response.coin.description,
            coinWebUrl = response.coin.websiteURL,
        )
        dialog.show(
            childFragmentManager, TAG
        )
    }

    companion object {
        private const val TAG = "ListOfCoinFragment"
    }
}