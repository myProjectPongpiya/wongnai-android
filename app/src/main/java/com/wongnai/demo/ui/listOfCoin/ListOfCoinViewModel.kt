package com.wongnai.demo.ui.listOfCoin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.wongnai.appdata.repository.model.coin.CoinDataResponseModel
import com.wongnai.appdata.repository.model.coin.CoinDetailResponseModel
import com.wongnai.appdata.repository.response.coin.CoinDetailResponse
import com.wongnai.appdata.repository.response.coin.CoinResponse
import com.wongnai.appdomain.usecase.coin.GetCoinDetailUseCase
import com.wongnai.appdomain.usecase.coin.GetCoinUseCase
import com.wongnai.appdomain.usecase.coin.SearchUseCase
import com.wongnai.appfoundation.base.BaseViewModel
import com.wongnai.appfoundation.extensions.doToggleLoadingStateOf
import com.wongnai.appfoundation.extensions.launchWith
import com.wongnai.demo.ui.listOfCoin.coinCard.CoinCardItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class ListOfCoinViewModel(
    private val getCoinUseCase: GetCoinUseCase,
    private val getCoinDetailUseCase: GetCoinDetailUseCase,
    private val searchUseCase: SearchUseCase
) : BaseViewModel() {

    private var countdownJob: Job? = null

    private val coinResponse = MutableLiveData<CoinDataResponseModel>()
    val coinList = Transformations.map(coinResponse, this::setCoinList)

    val coinDetailResponse = MutableLiveData<CoinDetailResponseModel>()

    val isGetCoinResponseEmpty = MutableLiveData(false)
    val isGetCoinResponseError = MutableLiveData(false)

    fun initialize() {
        onGetCoinUseCase()
    }

    private fun onGetCoinUseCase() {
        getCoinUseCase.build(Unit)
            .doToggleLoadingStateOf(this)
            .onEach { onGetCoinSuccess(it) }
            .launchWith(this, onError = {
                isGetCoinResponseError.value = true
                stopCooldown()
            })
    }

    private fun onGetCoinSuccess(response: CoinResponse) {
        coinResponse.value = response.data
        isGetCoinResponseEmpty.value = response.data.coins.isEmpty()
        startCooldown()
    }

    fun onGetCoinDetailUseCase(uuid: String) {
        getCoinDetailUseCase.build(uuid)
            .doToggleLoadingStateOf(this)
            .onEach { onGetCoinDetailSuccess(it) }
            .launchWith(this)
    }

    private fun onGetCoinDetailSuccess(response: CoinDetailResponse) {
        coinDetailResponse.value = response.data
    }

    fun onSearchUseCase(keyWord: String) {
        searchUseCase.build(keyWord)
            .doToggleLoadingStateOf(this)
            .onEach {
                onGetCoinSuccess(it)
                stopCooldown()
            }
            .launchWith(this)
    }

    private fun setCoinList(coinDataResponseModel: CoinDataResponseModel): List<CoinCardItem> {
        return coinDataResponseModel.coins.map {
            CoinCardItem(
                imgCoin = it.iconURL,
                tvNameCoin = it.name,
                tvABRVCoin = it.symbol,
                tvPriceCoin = it.btcPrice,
                change = it.change,
                uuid = it.uuid
            )
        }
    }

    fun onClickTryAgain() {
        isGetCoinResponseError.value = false
        startCooldown()
        onGetCoinUseCase()
    }

    fun onSwipeRefresh() {
        onGetCoinUseCase()
    }

    fun startCooldown() {
        countdownJob?.cancel()
        countdownJob = CoroutineScope(Dispatchers.IO).launch {
            delay(10000)
            onGetCoinUseCase()
        }
        countdownJob?.start()
    }

    private fun stopCooldown() = countdownJob?.cancel()
}