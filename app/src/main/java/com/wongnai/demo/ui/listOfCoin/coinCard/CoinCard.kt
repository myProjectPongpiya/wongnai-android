package com.wongnai.demo.ui.listOfCoin.coinCard

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import com.wongnai.appfoundation.list.DrBaseInfinityScrollList

class CoinCard @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
) : DrBaseInfinityScrollList(context, attrs, defStyle) {

    private val adapter = CoinCardListAdapter()

    init {
        super.initRecyclerView(
            adapter = adapter,
            layout = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false),
            decor = null
        )
    }

    /**
     * Sets the [items] to be displayed.
     */
    fun setItems(items: List<CoinCardItem>?) {
        adapter.setItems(items ?: emptyList())
    }

    fun setOnItemClickListener(listener: ((uuid: String) -> Unit)?) {
        adapter.setOnClickListener(listener)
    }
}