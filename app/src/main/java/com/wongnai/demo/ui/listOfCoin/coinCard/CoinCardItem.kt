package com.wongnai.demo.ui.listOfCoin.coinCard

data class CoinCardItem(
    val imgCoin: String? = null,
    val tvNameCoin: String? = null,
    val tvABRVCoin: String? = null,
    val tvPriceCoin: String? = null,
    val change: String? = null,
    val uuid: String? = null
)