package com.wongnai.demo.ui.listOfCoin.coinCard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.demo.R

class CoinCardListAdapter : RecyclerView.Adapter<CoinCardListViewHolder>() {

    private val items = mutableListOf<CoinCardItem>()

    private var onClickListener: ((uuid: String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinCardListViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_coin_card, parent, false)
        return CoinCardListViewHolder(view, this::onClickViewHolder)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CoinCardListViewHolder, position: Int) {
        when (position) {
            4, 9, 19, 39 -> holder.bindDataCardInvite()
            else -> holder.bindData(items[position])
        }
    }

    private fun onClickViewHolder(position: Int) {
        items[position].uuid?.let { onClickListener?.invoke(it) }
    }

    /**
     * Sets [items] in this [RecyclerView].
     */
    fun setItems(items: List<CoinCardItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnClickListener(listener: ((uuid: String) -> Unit)?) {
        this.onClickListener = listener
    }
}