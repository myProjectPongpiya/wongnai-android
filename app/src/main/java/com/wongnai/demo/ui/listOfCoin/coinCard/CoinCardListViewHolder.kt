package com.wongnai.demo.ui.listOfCoin.coinCard

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.wongnai.appfoundation.extensions.gone
import com.wongnai.appfoundation.extensions.loadUrl
import com.wongnai.appfoundation.extensions.removeDashes
import com.wongnai.appfoundation.extensions.visible
import com.wongnai.demo.R
import kotlinx.android.synthetic.main.item_coin_card.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class CoinCardListViewHolder(
    view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    override fun onClick(v: View?) {
        onClickListener.invoke(adapterPosition)
    }

    fun bindData(item: CoinCardItem) {
        itemView.cv1.visible()
        itemView.cv2.gone()

        item.imgCoin?.let {
            itemView.imgCoin.loadUrl(it)
        }
        item.tvNameCoin?.let {
            itemView.tvNameCoin.text = it
        }
        item.tvABRVCoin?.let {
            itemView.tvABRVCoin.text = it
        }
        item.tvPriceCoin?.let {
            itemView.tvPriceCoin.text = setDecimalFormat(it.toDouble())
        }
        item.change?.let {
            itemView.tvCoinChange.text = it.removeDashes()
            if (it.toDouble() > 0) {
                itemView.tvCoinChange.setTextColor(Color.GREEN)
                itemView.tvCoinChange.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_up,
                    0,
                    0,
                    0
                )
            } else {
                itemView.tvCoinChange.setTextColor(Color.RED)
                itemView.tvCoinChange.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_down,
                    0,
                    0,
                    0
                )
            }
        }

        itemView.setOnClickListener(this)
    }

    fun bindDataCardInvite() {
        itemView.cv1.visibility = View.GONE
        itemView.cv2.visibility = View.VISIBLE
    }

    private fun setDecimalFormat(number: Double): String {
        val df = DecimalFormat("#.#####")
        df.roundingMode = RoundingMode.FLOOR
        df.isDecimalSeparatorAlwaysShown = true
        val numberFormat = df.format(number).toString()
        return "$${numberFormat.toDouble()}"
    }
}